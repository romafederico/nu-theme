<?php get_header() ?>

<div class="main-content">
	<div class="row post-header hidden-xs hidden-sm">
		<div class="container">
			<h3>Pasillo Roverano</h3>
		</div>
	</div>
	<div class="container">
		<div class="row categoria">
			<div class="col-md-8">

				<?php

				$post_type = get_post_type(get_query_var('pasillo'));
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

				$args = array(
					'post_type' => $post_type,
					'posts_per_page' => 10,
					'paged' => $paged,
					'orderby' => 'date',
					'order' => 'DESC',
					'post_status' => 'publish'

				);

				$query = new WP_Query($args);

				if($query->have_posts()) :
					while($query->have_posts()) : $query->the_post();
						$postPermalink = get_post_permalink();
						$postDate = get_the_date();
						$postThumbnail = get_the_post_thumbnail_url(get_the_ID(), 'listados');
						$postTitle = get_the_title();
						$postCopete = get_post_meta(get_the_ID(), 'copete_meta_texto', true);
						echo '<a href="' . $postPermalink . '">';
						echo '<div class="cat-item">';
						echo '<img src="' . $postThumbnail .'" width="100%">';
						echo '</a>';
						echo '<div class="cat-date hidden-xs"><p>' . $postDate . '</p></div>';
            			echo '</div>';
						echo '<a href="' . $postPermalink . '">';
						echo '<div class="cat-item-title"><h3>' . $postTitle . '</h3>';
						echo '<p>' . $postCopete . '</p></div>';
						echo '</a>';
						echo '<div class="cat-item-footer"><div class="cat-item-footer-line"></div>';
						echo '<div class="cat-item-footer-social"><ul>';
						echo '<li><a href="https://facebook.com/sharer.php?&u=' . $postPermalink . '" target="_blank"><img src="' . get_template_directory_uri() . '/images/icon-share-fb.svg" height="23" alt="Facebook" /></a></li>';
						echo '<li><a href="https://twitter.com/share?url=' . $postPermalink . '" target="_blank"><img src="' . get_template_directory_uri() . '/images/icon-share-tw.svg" height="25" alt="Twitter"></a></li>';
						echo '<li><a href="whatsapp://send?text=' . $postPermalink . '" data-action="share/whatsapp/share"><img src="' . get_template_directory_uri() . '/images/icon-share-wapp.svg" height="25" alt="WhatsApp"></a></li>';
						echo '</ul></div></div>';
					endwhile;


					// next_posts_link('Older Entries', $query->max_num_pages);
					// previous_posts_link( 'Newer Entries');

					wp_reset_postdata();

				endif;
				?>
			</div>

			<?php get_sidebar(); ?>
		</div>
	</div>
</div>


<?php get_footer() ?>
