<?php get_header(); ?>

<div class="main-content">
	<div class="row post-header hidden-xs hidden-sm">
		<div class="container">
			<h3>
				Tapas de Revista NU
			</h3>
		</div>
	</div>
    <div class="container">
    <div class="row categoria">
        <div class="col-md-8">

			<?php

			$post_type = get_post_type(get_query_var('tapa'));
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

			$args = array(
				'post_type' => $post_type,
				'posts_per_page' => 36,
				'paged' => $paged,
				'orderby' => 'date',
				'order' => 'DESC',
                'post_status' => 'publish'

			);

			$query = new WP_Query($args);

			if($query->have_posts()) :
				while($query->have_posts()) : $query->the_post();
//
//					$postPermalink = get_post_permalink();
//					$postThumbnail = get_the_post_thumbnail_url(get_the_ID());
//					$postTitle = get_the_title();

					if(get_the_post_thumbnail()) {
                        echo '<div class="col-md-4 col-md-offset-1 tapalist-item"><a href="' . get_post_meta(get_the_ID(), 'link_meta_url', true) . '" target="_blank">' . get_the_post_thumbnail() . '</a></div>';
                    }


				endwhile;

				wp_reset_postdata();

			endif;
			?>
        </div>
	<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>