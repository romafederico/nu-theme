<?php get_header(); ?>

<?php
$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
$authorAvatar = get_avatar_url($curauth->ID);
$authorTwitter = get_the_author_meta('twitter', $curauth->ID);
?>

<div class="main-content">
	<div class="row post-header">
		<div class="container">
			<div class="post-author">
				<div class="post-author-avatar">
					<div class="author-photo" style="background-image: url(<?php echo $authorAvatar ?>)"></div>
				</div>
				<!--				<div class="post-author-name">Por --><?php //echo $authorFullName ?><!--</div>-->
				<div style="margin-bottom: 10px;" class="post-author-name"><h2> <?php echo $curauth->display_name; ?></h2>
                    <div>
                        <?php

                        if($authorTwitter) {
                            echo '<img style="margin-bottom: 5px; margin-right: 3px;" src="' . get_template_directory_uri() . '/images/icon-share-tw.png" height=\'15\' />';
                            echo '<a style="display: inline-block; margin-left:3px; margin-top: -20px;" href="http://www.twitter.com/' . $authorTwitter . '" target="_blank"><h4>' . $authorTwitter . '</h4></a>';
                        }

                        ?>
<!--                        <a style="display: inline-block; margin-left:3px;" href="--><?php //echo 'http://www.twitter.com/' . $authorTwitter; ?><!--" target="_blank"><h4>--><?php //echo $authorTwitter; ?><!--</h4></a>-->
                    </div>
                </div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row categoria">
			<div class="col-md-8">
				<?php

				$cat = get_category(get_query_var('cat'));
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

				$args = array(
					'post_type' => array('noticia', 'opinion', 'pasillo'),
					'author' => $curauth->ID,
					'posts_per_page' => 10,
					'category_name' => $cat->name,
					'paged' => $paged,
					'orderby' => 'date',
					'order' => 'DESC',
					'post_status' => 'publish',
                    'meta_query' => array(
                        array(
                            'key' => 'redaccion_meta_opcion',
                            'value' => 'autor',
                        ),
                    )
				);

				$query = new WP_Query($args);

				if($query->have_posts()) :
					while($query->have_posts()) : $query->the_post();
						$authorAvatar = get_avatar_url(get_the_author_meta('ID'));
						$authorFullName = get_the_author_meta('display_name', get_the_author_meta('ID'));
						$postPermalink = get_post_permalink();
						$postDate = get_the_date();
						$postThumbnail = get_the_post_thumbnail_url(get_the_ID(), 'listados');
						$postTitle = get_the_title();
						$postCopete = get_post_meta(get_the_ID(), 'copete_meta_texto', true);
						echo '<a href="' . $postPermalink . '">';
						echo '<div class="cat-item">';
						echo '<img src="' . $postThumbnail .'" width="100%">';
						echo '<div class="cat-date hidden-xs"><p>' . $postDate . '</p></div>';
						echo '</div>';
						echo '<div class="cat-item-title"><h3>' . $postTitle . '</h3>';
						echo '<p>' . $postCopete . '</p></div>';
						echo '<div class="cat-item-footer"><div class="cat-item-footer-line"></div>';
						echo '<div class="cat-item-footer-social"><ul>';
						echo '<li><a href=href="https://facebook.com/sharer/sharer.php?&u=' . $postPermalink . '" target="_blank"><img src="' . get_template_directory_uri() . '/images/icon-share-fb.svg" height="23" alt="Facebook" /></a></li>';
						echo '<li><a href="https://twitter.com/share?url=' . $postPermalink . '" target="_blank"><img src="' . get_template_directory_uri() . '/images/icon-share-tw.svg" height="25" alt="Twitter"></a></li>';
						echo '<li><a href="whatsapp://send?text='. $postPermalink. '" data-action="share/whatsapp/share"><img src="' . get_template_directory_uri() . '/images/icon-share-wapp.svg" height="25" alt="WhatsApp"></li>';
						echo '</ul></div></div>';
						echo '</a>';
					endwhile;

					wp_reset_postdata();

					// next_posts_link('Older Entries',$query->max_num_pages);
					// previous_posts_link('Newer Entries');

				endif;
				?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
