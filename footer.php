</div><!-- /.row -->

</div><!-- /.container -->

<footer class="footer">
  	<div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
		<div class="row">
			<div class="navbar-header">
				<!-- <a class="visible-xs" href="<?= get_bloginfo('url'); ?>"><img src="<?php bloginfo('stylesheet_directory');?>/images/icon-nu.svg" height="50" alt="Noticias Urbanas"></a> -->
				<a href="<?= get_bloginfo('url'); ?>"><img src="<?php bloginfo('stylesheet_directory');?>/images/nu_brand.svg" width="300px" alt="Noticias Urbanas"></a>
			</div>

			<div class="hidden-xs">
				<ul class="nav-social">
					<li class="nav-social-item"><a href="https://www.facebook.com/noticiasurbanas"><img src="<?php bloginfo('stylesheet_directory');?>/images/icon-fb.svg" height="35" alt="Facebook"></a></li>
					<li class="nav-social-item"><a href="https://twitter.com/noticiasurbanas"><img src="<?php bloginfo('stylesheet_directory');?>/images/icon-tw.svg" height="35" alt="Twitter"></a></li>
					<li class="nav-social-item"><a href="https://www.instagram.com/noticias.urbanas/"><img src="<?php bloginfo('stylesheet_directory');?>/images/icon-ig.svg" height="35" alt="Instagram"></a></li>
					<li class="nav-social-item"><a href="https://www.youtube.com/user/NoticiasUrbanasSRL"><img src="<?php bloginfo('stylesheet_directory');?>/images/icon-yt.svg" height="35" alt="Youtube"></a></li>
				</ul>
			</div><!-- /.navbar-collapse -->

		</div>
		Avenida de Mayo 560 piso 4. Oficina 37. CP: 1084. Ciudad Autónoma de Buenos Aires, República Argentina.<br>
		Teléfonos: (005411) 4343-6240 / 0800-333-4755.<br>
		Correo: info@noticiasurbanas.com.ar<br>
		Propietario: Producciones Urbanas S.R.L<br>
		Dirección Nacional del Derecho de Autor Nro. 5280058 <br>
	</div><!-- /.container -->
</footer>
<?php wp_footer(); ?>
<?php echo stripslashes(theme_options('tracking')); // tracking code ?>
</body>
</html>
