<?php


// Bootstrap starter
function bootstrapstarter_enqueue_styles()
{
    wp_register_style('bootstrap', get_template_directory_uri() . '/bootstrap/css/bootstrap.css');
    $dependencies = array('bootstrap');
    wp_enqueue_style('bootstrapstarter-style', get_stylesheet_uri(), $dependencies);
}

function bootstrapstarter_enqueue_scripts()
{
    $dependencies = array('jquery');
    wp_enqueue_script('bootstrap', get_template_directory_uri().'/bootstrap/js/bootstrap.js', $dependencies, '3.3.7', true);
}

function bootstrapstarter_wp_setup()
{
    add_theme_support('title-tag');
}

add_action('after_setup_theme', 'bootstrapstarter_wp_setup');
add_action('wp_enqueue_scripts', 'bootstrapstarter_enqueue_styles');
add_action('wp_enqueue_scripts', 'bootstrapstarter_enqueue_scripts');

// Include Theme options
include(TEMPLATEPATH . "/includes/theme-options.php");

//  Pull theme options from database
function theme_options($key) {
    global $settings;
    $option = get_option($settings);
    if (isset($option[$key]))
        return $option[$key];
    else
        return NULL;
}


// Include and Init Post Types
include(TEMPLATEPATH . "/includes/posttypes.php");
add_action('init', 'configure_post_types');

// Include Custom Metaboxes
include(TEMPLATEPATH . "/includes/metaboxes.php");

// Init Custom Metaboxes
add_action('add_meta_boxes', 'register_all_metaboxes');

// Save Data from Custom Metaboxes
function save_custom_metaboxes($post_id)
{
    save_copete_metabox($post_id);
    save_subtitulo_metabox($post_id);
    save_redaccion_metabox($post_id);
    save_galeria_metabox($post_id);
    save_link_metabox($post_id);
    save_banner_metabox($post_id);
    save_video_metabox($post_id);
}
add_action('save_post', 'save_custom_metaboxes');

// Include Custom widgets
include(TEMPLATEPATH . "/includes/widgets.php");

// Init Custom Widgets
add_action('widgets_init', 'register_all_widgets');

// Init Noticias Box
function getNoticias($post, $imageSize, $color) {
	$postType = get_post_type($post);
	$authorType = get_post_meta($post,'redaccion_meta_opcion',true);

	if($authorType == 'autor'){
		$author = get_the_author_meta('display_name', get_post_field( 'post_author', $post));
		$authorUrl = get_author_posts_url(get_post_field( 'post_author', $post));
	} else {
		$author = theme_options('redaccion');
	}

	$permalink = get_post_permalink($post);
	$copete = get_post_meta($post, 'copete_meta_texto', true);

	if($postType == 'pasillo') {
	    $imageSize = 'pasillo-' . $imageSize;
    }

    switch ($color) {
        case 1:
            $background = '107,189,81';
            break;
        case 2:
            $background = '218,148,79';
            break;
        case 3:
            $background = '58,194,225';
            break;
    }

	switch ($postType) {
        case 'noticia':
            ?>

            <div class="noticias">
                <a href="<?php echo $permalink ?>">
                    <div class="noticias-copete"><h4><?php echo $copete ?></h4></div>
                    <div class="hidden-xs hidden-sm"><img src="<?php echo get_the_post_thumbnail_url($post, $imageSize) ?>" width="100%"></div>
                    <div class="hidden-md hidden-lg"><img src="<?php echo get_the_post_thumbnail_url($post, 'col6') ?>" width="100%"></div>
                </a>
                <div style="background-color:rgba(<?php echo $background?>,1);" class="noticias-itembox">
                    <a href="<?php echo $permalink ?>"><h4><?php echo get_the_title($post) ?></h4></a>
                    <div class='noticias-author'>
                        <?php
                            if ($authorType == 'autor') {
                                echo '<a href="' . $authorUrl . '"><h5 style="color: white;">Por ' . $author . '</h5></a>';
                            }
                        ?>
                    </div>
                    <ul class='noticias-icon-share'>
                        <li><a href='https://www.facebook.com/sharer/sharer.php?&u=<?php echo $permalink ?>' target='_blank'><img src='./wp-content/themes/nu-theme/images/icon-share-fb.png' height='15' /></a></li>
                        <li><a href='https://twitter.com/share?url=<?php echo $permalink ?>' target='_blank'><img src='./wp-content/themes/nu-theme/images/icon-share-tw.png' height='15' /></a></li>
                        <li><a href='whatsapp://send?text=<?php echo $permalink ?>' data-action="share/whatsapp/share"><img src='./wp-content/themes/nu-theme/images/icon-share-wapp.png' height='15' /></a></li>
                    </ul>
                </div>
            </div>

            <?php
            break;

        case 'pasillo';
            ?>
            <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
            <div class="pasillo">
                <div style="font-family: Yellowtail; font-size: 1.5em;" class="pasillo-header">Pasillo Roverano</div>
                <a href="<? echo $permalink ?>">
                    <div class="hidden-xs hidden-sm"><img src="<?php echo get_the_post_thumbnail_url($post, $imageSize) ?>"></div>
                    <div class="hidden-md hidden-lg"><img src="<?php echo get_the_post_thumbnail_url($post, 'pasillo-col6') ?>" width="100%"></div>
                </a>
                <div class="pasillo-itembox">
                    <a href="<? echo $permalink ?>"><h3><?php echo get_the_title($post) ?></h3></a>
                </div>
            </div>

            <?php
            break;

    }

}

// Init Noticias Share Box
function getNoticiasShareBox($postUrl) {
	$shareBox = "";
	$shareBox .= "<ul class='noticias-icon-share'>";
	$shareBox .= "<li><a href='https://www.facebook.com/sharer/sharer.php?&u=$postUrl' target='_blank'><img src='./wp-content/themes/nu-theme/images/icon-share-fb.png' height='15' /></a></li>";
    $shareBox .= "<li><a href='https://twitter.com/share?url=$postUrl' target='_blank'><img src='./wp-content/themes/nu-theme/images/icon-share-tw.png' height='15' /></a></li>";
    $shareBox .= "<li><a href=\"whatsapp://send?text=$postUrl\" data-action=\"share/whatsapp/share\"><img src='./wp-content/themes/nu-theme/images/icon-share-wapp.png' height='15' /></a></li>";
    $shareBox .= "</ul>";

    return $shareBox;
}

// Disable Default Admin Menus
function remove_admin_bar_nodes( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
	$wp_admin_bar->remove_node( 'new-content' );
	$wp_admin_bar->remove_node( 'comments' );
	$wp_admin_bar->remove_node( 'archive' );
	$wp_admin_bar->remove_node( 'updates' );
	$wp_admin_bar->remove_node( 'wpseo-menu' );
}

function configure_admin_menu()  {
	$current_user = wp_get_current_user();
	if ($current_user->user_email !== 'romafederico@gmail.com') {
		add_menu_page('Widgets', 'Widgets', 'edit_theme_options', 'widgets.php','','dashicons-text');
		add_menu_page('Live Preview', 'Live Preview', 'edit_theme_options', 'customize.php','','dashicons-welcome-view-site');
		remove_menu_page('index.php');
		remove_menu_page('tools.php');
		remove_menu_page('plugins.php');
		remove_menu_page('themes.php');
		remove_menu_page('options-general.php');
    	remove_menu_page('edit.php?post_type=page');
		remove_menu_page('cerber-security');
		remove_menu_page('wpseo_dashboard');
		remove_menu_page('edit.php?post_type=wpc_weather');
		remove_action( 'admin_notices', 'update_nag', 3 );
		remove_submenu_page('themes.php', 'nav-menus.php');
		remove_submenu_page('themes.php', 'theme-editor.php');
		add_action( 'admin_bar_menu', 'remove_admin_bar_nodes', 999 );
	}
	remove_menu_page('edit-comments.php');
	remove_menu_page('link-manager.php');
	remove_menu_page('edit.php');
}

add_action('admin_menu', 'configure_admin_menu');


// Login Redirect
function loginRedirect( $redirect_to, $request, $user ){
	if( is_array( $user->roles ) ) { // check if user has a role
		return "./wp-admin/edit.php?post_type=noticia";
	}
}
add_filter("login_redirect", "loginRedirect", 10, 3);

// Hide SuperAdmin from admins
add_action('pre_user_query','yoursite_pre_user_query');
function yoursite_pre_user_query($user_search) {
	global $current_user;
	$username = $current_user->user_login;

	if ($username != 'romafederico') {
		global $wpdb;
		$user_search->query_where = str_replace('WHERE 1=1',
			"WHERE 1=1 AND {$wpdb->users}.user_login != 'romafederico'",$user_search->query_where);
	}
}

// Remove edit permalink from Banners
function posttype_admin_css()  {
    global $post_type;
    if ($post_type == 'banner' || $post_type == 'opinion' || $post_type == 'pasillo' || $post_type == 'chicanu' || $post_type == 'chiconu' || $post_type == 'tapa' || $post_type == 'video') {
        echo '<style type="text/css">#edit-slug-box,#view-post-btn,#post-    preview,.updated p a{display: none;}</style>';
    }
}
add_action('admin_head', 'posttype_admin_css');

// Include HTML Banners Functions
include(TEMPLATEPATH . "/includes/htmlbanners.php");

//// Allow SWF Files
function allow_swf_uploads($mimes) {
    if ( function_exists( 'current_user_can' ) )
        $unfiltered = $user ? user_can( $user, 'unfiltered_html' ) : current_user_can( 'unfiltered_html' );
    if ( !empty( $unfiltered ) ) {
        $mimes['swf'] = 'application/x-shockwave-flash';
        $mimes['zip'] = 'application/zip';
    }
    return $mimes;
}
add_filter('upload_mimes','allow_swf_uploads');


// Function to Autocomplete Tag List
function autocomplete_taglist(){
    $result = array();
    $my_query = array('search'=>$_GET['term']);
    if(isset($_GET['exclude']) && $_GET['exclude']!=''):
        $my_query['exclude'] = explode('-',$_GET['exclude']);
    endif;
    $tags = get_tags($my_query);

    foreach($tags as $tag):
        $result[] = array('label'=>$tag->name,'id'=>$tag->term_id);
    endforeach;
    echo json_encode($result);
    die();
}

add_action('wp_ajax_autocomplete_taglist', 'autocomplete_taglist');

// Post Images Configuration
add_theme_support('post-thumbnails');

add_image_size( 'portada-principal', 758, 442, true );
add_image_size( 'portada-secundaria', 378, 160, true );
add_image_size( 'portada-interactiva', 378, 125, true );
add_image_size( 'col6', 568, 300, true );
add_image_size( 'col4', 378, 300, true );
add_image_size( 'col3', 284, 300, true );

add_image_size( 'pasillo-portada-principal', 738, 542, true );
add_image_size( 'pasillo-portada-secundaria', 358, 260, true );
add_image_size( 'pasillo-col6', 548, 400, true );
add_image_size( 'pasillo-col4', 358, 400, true );
add_image_size( 'pasillo-col3', 264, 400, true );

add_image_size( 'tapa', 238, 340, true );

add_image_size( 'secundarias', 350, 220, true );
add_image_size( 'listados', 750, 380, true );
add_image_size( 'chicxs', 568, 420, true );
add_image_size( 'single', 750, 500, true );
add_image_size( 'sugeridas', 280, 160, true );

// In-post responisve images
function add_image_responsive_class($content) {
    global $post;
    $pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
    $replacement = '<img$1class="$2 img-responsive"$3>';
    $content = preg_replace($pattern, $replacement, $content);
    return $content;
}
add_filter('the_content', 'add_image_responsive_class');


// Init Weather
register_activation_hook(__FILE__, 'weatherActivation');

function weatherActivation() {
	if (! wp_next_scheduled ( 'hourly-weather-check' )) {
		wp_schedule_event(time(), 'hourly', 'hourly-weather-check');
	}
}

add_action('hourly-weather-check', 'fetchWeather');

function fetchWeather() {
	$url      = 'http://api.openweathermap.org/data/2.5/weather?id=3433955&units=metric&lang=es&APPID=a2ae0a806ac9ff6fa71962436388ce43';
	$response = wp_remote_get( esc_url_raw( $url ) );
	$api_response = json_decode( wp_remote_retrieve_body( $response ), true );
	update_option('nu-weather', $api_response);
}

function getWeather() {
	return get_option('nu-weather');
}

// Init Edition Number

register_activation_hook(__FILE__, 'editionActivation');

function editionActivation() {
	if (! wp_next_scheduled ( 'daily-edition-check' )) {
		wp_schedule_event(time(), 'daily', 'daily-edition-check');
	}
}

add_action('daily-edition-check', 'addEdition');

function addEdition() {
	$currentEdition = getEdition();
	$newEdition = $currentEdition + 1;
	update_option('nu-edition', $newEdition);
}

function getEdition() {
	return get_option('nu-edition');
}

// Custom Fields

add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

function my_show_extra_profile_fields( $user ) { ?>

	<h3>Más información de usuarios NU</h3>

	<table class="form-table">

		<tr>
			<th><label for="phone">Teléfono</label></th>

			<td>
				<input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Ingresa tu numero de teléfono.</span>
			</td>
		</tr>

		<tr>
			<th><label for="twitter">Twitter</label></th>

			<td>
				<input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Ingresa tu usuario de twitter.</span>
			</td>
		</tr>

        <tr>
            <th><label for="jerarquia">Staff NU - Jerarquía</label></th>

            <td>
                <input type="number" name="jerarquia" id="jerarquia" value="<?php echo esc_attr( get_the_author_meta( 'jerarquia', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description"><strong>Ingresar SOLO números,</strong> siendo 1 es la posición más importante en NU. <strong>Si el usuario NO es parte del Staff NU,</strong> entonces poner 0 (cero).</span>
            </td>
        </tr>
        <tr>
            <th><label for="cargo">Cargo</label></th>

            <td>
                <input type="text" name="cargo" id="cargo" value="<?php echo esc_attr( get_the_author_meta( 'cargo', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description"><strong>Ingresar cargo de la persona</span>
            </td>
        </tr>

	</table>
<?php
}

add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	/* Copy and paste this line for additional fields. Make sure to change 'twitter' to the field ID. */
	update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
	update_user_meta( $user_id, 'phone', $_POST['phone'] );
	update_user_meta( $user_id, 'staff', $_POST['staff'] );
	update_user_meta( $user_id, 'jerarquia', sprintf('%03d', $_POST['jerarquia'])  );
	update_user_meta( $user_id, 'cargo', $_POST['cargo'] );
}

// Custom Capabilites
function add_theme_caps() {
	// gets the author role
	$role = get_role( 'author' );

	// This only works, because it accesses the class instance.
	// would allow the author to edit others' posts for current theme only
	$role->add_cap( 'edit_theme_options' );
}
add_action( 'admin_init', 'add_theme_caps');


//// Post Views
add_action( 'wp_enqueue_scripts', 'process_post_views_with_wp_cache_enabled' );
function process_post_views_with_wp_cache_enabled() {
    global $post;
    if ( is_int( $post ) ) {
        $post = get_post( $post );
    }

    if ( ! defined( 'WP_CACHE' ) || ! WP_CACHE ) {
        return;
    }

    if ( wp_is_post_revision( $post ) || is_preview() ) {
        return;
    }

    if ( ! is_single() && ! is_page() ) {
        return;
    }

    wp_enqueue_script( 'post-views-cache', get_template_directory_uri() . '/js/post-views-cache.js', array( 'jquery' ), '1.0', true );

    wp_localize_script( 'post-views-cache', 'PostViewsCache', array(
        'ajaxurl'  => admin_url( 'admin-ajax.php' ),
        'post_id'  => intval( $post->ID ),
    ) );
}

// Ajax handler to process post views with WP_CACHE enabled
add_action( 'wp_ajax_process_postviews', 'process_postviews' );
add_action( 'wp_ajax_nopriv_process_postviews', 'process_postviews' );
function process_postviews() {
    if ( ! defined( 'WP_CACHE' ) || ! WP_CACHE ) {
        return;
    }

    if ( empty( $_GET['post_id'] ) ) {
        return;
    }

    $post_id = intval( $_GET['post_id'] );
    if ( $post_id > 0 ) {
        $post_views = get_post_meta( $post_id, 'views', true );
        if ( ! $post_views ) {
            $post_views = 0;
        }
        update_post_meta( $post_id, 'views', ( $post_views + 1 ) );
        echo ( $post_views + 1 );
    }
    exit();
}


//// function to display number of posts.
function getPostViews($postID){
    $count_key = 'views';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return '0';
    }
    return $count;
}


//// Add it to a column in WP-Admin - (Optional)
add_filter('manage_posts_columns', 'posts_column_views');
add_action('manage_posts_custom_column', 'posts_custom_column_views',5,2);
function posts_column_views($defaults){
    $defaults['post_views'] = __('Vistas');
    return $defaults;
}
function posts_custom_column_views($column_name, $id){
    if($column_name === 'post_views'){
        echo getPostViews(get_the_ID());
    }
}

add_filter('upload_mimes', 'mqw_mas_extensiones');
function mqw_mas_extensiones ( $existing_mimes=array() ) {
    $existing_mimes['swf'] = 'application/x-shockwave-flash';
    return $existing_mimes;
}

// Autocomplete

function configure_admin_scripts() {
    wp_enqueue_script('nu-theme', get_bloginfo('template_url') . '/js/nulogic.js', array('jquery'));
    wp_enqueue_script('jquery-ui-autocomplete');
}

add_action('admin_enqueue_scripts', 'configure_admin_scripts');

function autocomplete_widgets(){
    $result = array();
    $post_type=(isset($_GET['post_type']))?$_GET['post_type']:array('noticia','opinion','chicanu','chiconu','pasillo');

    $my_query = array(
        's'=>$_GET['term'],
        'post_type'=>$post_type,
        'numberposts'=>50,
        'post_status'=>'published',
        'orderby'=>'post_date',
        'order'=>'DESC'
    );

    if($_GET['exclude']):
        $my_query['exclude'] = explode('-',$_GET['exclude']);
    endif;
    $posts = get_posts($my_query);

    foreach($posts as $post):
        $result[] = array('label'=>$post->post_title,'id'=>$post->ID);
    endforeach;

    echo json_encode($result);
    die();
}

add_action('wp_ajax_autocomplete_widgets', 'autocomplete_widgets');