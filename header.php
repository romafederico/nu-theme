<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,800|Open+Sans:400,700,800" rel="stylesheet">
    <script src="https://use.fontawesome.com/580504af78.js"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <link type="text/css" rel="stylesheet" href="<?php bloginfo('stylesheet_directory');?>/css/lightslider.css" />
    <script src="<?php bloginfo('stylesheet_directory');?>/js/lightslider.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

    <div id="fb-root"></div>

    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9&appId=138217189681994";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <nav class="navbar navbar-default">
        <?php
        $weather = getWeather();
        $edition = getEdition();
        ?>

        <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header">
                <div class="nu-logo">
                    <a class="hidden-xs hidden-sm" href="<?= get_bloginfo('url'); ?>"><img src="<?php bloginfo('stylesheet_directory');?>/images/icon-nu.svg" height="60" alt="Noticias Urbanas"><h2>Noticias Urbanas</h2></a>
                    <a class="visible-xs visible-sm mobile" href="<?= get_bloginfo('url'); ?>"><img src="<?php bloginfo('stylesheet_directory');?>/images/icon-nu.svg" height="50" alt="Noticias Urbanas"><h3 style="display: inline-block; color: #333333; margin-left: 10px; margin:0 0 0 10px; vertical-align: middle;">Noticias Urbanas</h3></a>
                </div>

                <div class="nu-data hidden-xs hidden-sm">
                    <span class="time"><?php echo date_i18n('g:i a') ?></span> |
                    <span class="date"><?php echo date_i18n('j \d\e F \d\e\ Y') ?></span> |
                    <span class="edition">Edición N˚'<?php echo $edition ?></span>
                    <div class="hidden-xs hidden-sm search-container">
                        <form action="<?= get_bloginfo('url') ?>" method="get" id="searchform">
                            <input type="text" value="" name="s">
                            <input type="hidden" name="post_type[]" value="noticias" />
                            <button class="btn btn-xs btn-primary" type="submit">Buscar</button>
                        </form>
                    </div>
                </div>

                <div class="weather hidden-xs hidden-sm">
                    <div class="icon" style="background-image: url('http://openweathermap.org/img/w/<?php echo $weather['weather'][0]['icon'] ?>.png')"></div>
                    <span class="temp"><?php echo $weather['main']['temp'] ?>˚C</span>
                </div>

                <div class="social-icons hidden-xs hidden-sm">
                    <ul class="nav-social">
                        <li class="nav-social-item"><a target="_blank" href="https://www.facebook.com/noticiasurbanas/"><img src="<?php bloginfo('stylesheet_directory');?>/images/icon-fb.svg" height="40" alt="Facebook"></a></li>
                        <li class="nav-social-item"><a target="_blank" href="https://www.twitter.com/noticiasurbanas/"><img src="<?php bloginfo('stylesheet_directory');?>/images/icon-tw.svg" height="40" alt="Twitter"></a></li>
                        <li class="nav-social-item"><a target="_blank" href="https://www.instagram.com/noticias.urbanas/"><img src="<?php bloginfo('stylesheet_directory');?>/images/icon-ig.svg" height="40" alt="Instagram"></a></li>
                        <li class="nav-social-item"><a target="_blank" href="https://www.youtube.com/user/NoticiasUrbanasSRL"><img src="<?php bloginfo('stylesheet_directory');?>/images/icon-yt.svg" height="40" alt="Youtube"></a></li>
                    </ul>
                </div>

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <script>
                $(function(){

                    $('li.dropdown > a').each(function () {
                        var width = $('ul.navbar-nav').width()
                        var position = $('ul.navbar-nav').position()
                        $(this).next('.dropdown-menu').css({
                            'width': width,
                            'padding-top': '1em',
                            'padding-bottom': '1em',
//                            'left': position.left
                        });
                    });

                })
            </script>

            <li class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="nav-item dropdown">
                        <a href="<?=get_category_link(get_cat_ID("ciudad"));?>" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">CIUDAD</a>
                        <ul class="dropdown-menu">
                            <?php
                                $args = array(
                                    'post_type' => 'noticia',
                                    'posts_per_page' => 5,
                                    'category_name' => 'ciudad',
                                    'orderby' => 'date',
                                    'order' => 'DESC',
                                    'post_status' => 'publish'
                                );
                                $query = new WP_Query($args);
                                if($query->have_posts()) :
                                    while($query->have_posts()) : $query->the_post();

                                        $postThumbnail = get_the_post_thumbnail_url(get_the_ID(), 'col6');
                                        $postPermalink = get_post_permalink();
                                        $postDate = ucfirst(get_the_date());
                                        $postTitle = get_the_title();

                                        echo '<a href="'.$postPermalink.'"><li><img src="'.$postThumbnail.'" width="100%"><span>'.$postDate.'</span><h4>'.$postTitle.'</h4></li></a>';
                                    endwhile;
                                    wp_reset_postdata();
                                endif;
                            ?>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?=get_category_link(get_cat_ID("provincia"));?>" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">PROVINCIA</a>
                        <ul class="dropdown-menu">
			                    <?php
			                    $args = array(
				                    'post_type' => 'noticia',
				                    'posts_per_page' => 5,
				                    'category_name' => 'provincia',
				                    'orderby' => 'date',
				                    'order' => 'DESC',
				                    'post_status' => 'publish'
			                    );
			                    $query = new WP_Query($args);
			                    if($query->have_posts()) :
				                    while($query->have_posts()) : $query->the_post();

					                    $postThumbnail = get_the_post_thumbnail_url(get_the_ID(), 'col6');
					                    $postPermalink = get_post_permalink();
					                    $postDate = ucfirst(get_the_date());
					                    $postTitle = get_the_title();

					                    echo '<a href="'.$postPermalink.'"><li><img src="'.$postThumbnail.'" width="100%"><span>'.$postDate.'</span><h4>'.$postTitle.'</h4></li></a>';
				                    endwhile;
				                    wp_reset_postdata();
			                    endif;
			                    ?>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?=get_category_link(get_cat_ID("nacion"));?>" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">NACIÓN</a>
                        <ul class="dropdown-menu">
			                    <?php
			                    $args = array(
				                    'post_type' => 'noticia',
				                    'posts_per_page' => 5,
				                    'category_name' => 'nacion',
				                    'orderby' => 'date',
				                    'order' => 'DESC',
				                    'post_status' => 'publish'
			                    );
			                    $query = new WP_Query($args);
			                    if($query->have_posts()) :
				                    while($query->have_posts()) : $query->the_post();

					                    $postThumbnail = get_the_post_thumbnail_url(get_the_ID(), 'col6');
					                    $postPermalink = get_post_permalink();
					                    $postDate = ucfirst(get_the_date());
					                    $postTitle = get_the_title();

					                    echo '<a href="'.$postPermalink.'"><li><img src="'.$postThumbnail.'" width="100%"><span>'.$postDate.'</span><h4>'.$postTitle.'</h4></li></a>';
				                    endwhile;
				                    wp_reset_postdata();
			                    endif;
			                    ?>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?=get_category_link(get_cat_ID("revista"));?>" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">REVISTA</a>
                        <ul class="dropdown-menu">
	                        <?php
	                        $args = array(
		                        'posts_per_page' => 5,
		                        'post_type' => 'tapa',
		                        'orderby' => 'date',
		                        'order' => 'DESC',
		                        'post_status' => 'publish'
	                        );

	                        $query = new WP_Query($args);
	                        if($query->have_posts()) :
		                        while($query->have_posts()) : $query->the_post();
			                        ?>

                                    <a href="<?php echo get_post_meta(get_the_ID(), 'link_meta_url', true); ?>" target="_blank">
                                        <li style="text-align: center">
                                            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'tapa'); ?>" width="100%">
                                            <h4><?php echo get_the_title(); ?></h4>
                                        </li>
                                    </a>

			                        <?php

		                        endwhile;
		                        wp_reset_postdata();
	                        endif;

	                        ?>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?=get_post_type_archive_link("opinion");?>" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">OPINIÓN</a>
                        <ul class="dropdown-menu">
	                        <?php

                            $args = array(
                                'post_type' => 'opinion',
                                'posts_per_page' => 5,
                                'orderby' => 'date',
                                'order' => 'DESC',
                                'post_status' => 'publish'
                            );
                            $query = new WP_Query($args);

	                        if($query->have_posts()) :
		                        while($query->have_posts()) : $query->the_post();
			                        $authorAvatar = get_avatar_url(get_the_author_meta('ID'));
			                        $authorFullName = get_the_author_meta('display_name', get_the_author_meta('ID'));
			                        $postTitle = get_the_title();

			                        ?>

                                    <a href="<?php echo get_permalink() ?>" style="flex: 1;">
                                        <li>
                                            <div class="dropdown-menu-author-photo" style="background-image: url('<?php echo $authorAvatar ?>')"></div>
                                            <h4><?php echo $postTitle ?></h4>
                                            <h5>Por <?php echo $authorFullName ?></h5>
                                        </li>
                                    </a>

			                        <?php

		                        endwhile;
		                        wp_reset_postdata();
	                        endif;

	                        ?>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?=get_post_type_archive_link("pasillo");?>" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">PASILLO ROVERANO</a>
                        <ul class="dropdown-menu">
                            <?php
                                $args = array(
                                    'post_type' => 'pasillo',
                                    'posts_per_page' => 5,
                                    'orderby' => 'date',
                                    'order' => 'DESC',
                                    'post_status' => 'publish'
                                );
                                $query = new WP_Query($args);
                                if($query->have_posts()) :
                                    while($query->have_posts()) : $query->the_post();

                                        $postThumbnail = get_the_post_thumbnail_url(get_the_ID(), 'col6');
                                        $postPermalink = get_post_permalink();
                                        $postDate = ucfirst(get_the_date());
                                        $postTitle = get_the_title();

                                        echo '<a href="'.$postPermalink.'"><li><img src="'.$postThumbnail.'" width="100%"><span>'.$postDate.'</span><h4>'.$postTitle.'</h4></li></a>';
                                    endwhile;
                                    wp_reset_postdata();
                                endif;
                            ?>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?=get_category_link(get_cat_ID("turismo"));?>" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">TURISMO</a>
                        <ul class="dropdown-menu">
                            <?php
                                $args = array(
                                    'post_type' => 'noticia',
                                    'posts_per_page' => 5,
                                    'category_name' => 'turismo',
                                    'orderby' => 'date',
                                    'order' => 'DESC',
                                    'post_status' => 'publish'
                                );
                                $query = new WP_Query($args);
                                if($query->have_posts()) :
                                    while($query->have_posts()) : $query->the_post();

                                        $postThumbnail = get_the_post_thumbnail_url(get_the_ID(), 'col6');
                                        $postPermalink = get_post_permalink();
                                        $postDate = ucfirst(get_the_date());
                                        $postTitle = get_the_title();

                                        echo '<a href="'.$postPermalink.'"><li><img src="'.$postThumbnail.'" width="100%"><span>'.$postDate.'</span><h4>'.$postTitle.'</h4></li></a>';
                                    endwhile;
                                    wp_reset_postdata();
                                endif;
                            ?>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="/staff-nu">STAFF NU</a>
                        <ul class="dropdown-menu">
	                        <?php

	                        $args = array(
		                        'include' => [189, 117, 20],
                                'orderby' => 'id',
                                'order' => 'DESC'
	                        );

	                        $allUsers = get_users($args);

	                        foreach($allUsers as $user) {
		                        $userName = $user->display_name;

                                ?>
                                <li>
                                    <div class="staff-item">
                                      <a href="<?php echo get_author_posts_url($user->ID) ?>">
                                          <div class="opi-author-avatar"><div class="opi-author-photo" style="background-image: url('<?php echo get_avatar_url($user->ID) ?>')"></div></div>
                                          <h3><?php echo esc_html( $user->display_name ) ?></h3>
                                      </a>
                                      <h4><?php echo get_user_meta($user->ID, 'cargo', true); ?></h4>
                                    </div>
                                </li>
		                        <?php
	                        }
	                        ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
