<?php
// Include Custom Metaboxes
include(TEMPLATEPATH . "/includes/metaboxes/copete.metabox.php");
include(TEMPLATEPATH . "/includes/metaboxes/banner.metabox.php");
include(TEMPLATEPATH . "/includes/metaboxes/subtitulo.metabox.php");
include(TEMPLATEPATH . "/includes/metaboxes/redaccion.metabox.php");
include(TEMPLATEPATH . "/includes/metaboxes/galeria.metabox.php");
include(TEMPLATEPATH . "/includes/metaboxes/formato.metabox.php");
include(TEMPLATEPATH . "/includes/metaboxes/link.metabox.php");
//include(TEMPLATEPATH . "/includes/metaboxes/postid.metabox.php");
include(TEMPLATEPATH . "/includes/metaboxes/video.metabox.php");

// Register Custom Metabox
function register_all_metaboxes()
{
    // Metaboxes Noticia
//    add_meta_box('postid_metabox', 'Post ID', 'show_postid_metabox', 'noticia', 'side', 'high');
    add_meta_box('redaccion_metabox', 'Autor/Redacción', 'show_redaccion_metabox', 'noticia', 'side', 'high');
    add_meta_box('subtitulo_metabox', 'Subtítulo', 'show_subtitulo_metabox', 'noticia', 'normal', 'high', null);
    add_meta_box('copete_metabox', 'Copete', 'show_copete_metabox', 'noticia', 'normal', 'high', null);
    add_meta_box('galeria_metabox', 'Galeria de Fotos', 'show_galeria_metabox', 'noticia', 'normal', 'high');
    add_meta_box('formato_metabox', 'Formato de Imagenes', 'show_formato_metabox', 'noticia', 'side', 'default');

    // Metaboxes Opinion
//    add_meta_box('postid_metabox', 'Post ID', 'show_postid_metabox', 'opinion', 'side', 'high');
    add_meta_box('redaccion_metabox', 'Autor/Redacción', 'show_redaccion_metabox', 'opinion', 'side', 'high');
    add_meta_box('subtitulo_metabox', 'Subtítulo', 'show_subtitulo_metabox', 'opinion', 'normal', 'high', null);
    add_meta_box('copete_metabox', 'Copete', 'show_copete_metabox', 'opinion', 'normal', 'high', null);

    // Metaboxes Pasillo
//    add_meta_box('postid_metabox', 'Post ID', 'show_postid_metabox', 'pasillo', 'side', 'high');
    add_meta_box('redaccion_metabox', 'Autor/Redacción', 'show_redaccion_metabox', 'pasillo', 'side', 'high');
    add_meta_box('subtitulo_metabox', 'Subtítulo', 'show_subtitulo_metabox', 'pasillo', 'normal', 'high', null);
    add_meta_box('copete_metabox', 'Copete', 'show_copete_metabox', 'pasillo', 'normal', 'high', null);
    add_meta_box('galeria_metabox', 'Galeria de Fotos', 'show_galeria_metabox', 'pasillo', 'normal', 'high');
    add_meta_box('formato_metabox', 'Formato de Imagenes', 'show_formato_metabox', 'pasillo', 'side', 'default');


    // Metaboxes Chicas Nu
//    add_meta_box('postid_metabox', 'Post ID', 'show_postid_metabox', 'chicanu', 'side', 'high');
    add_meta_box('galeria_metabox', 'Galeria de Fotos', 'show_galeria_metabox', 'chicanu', 'normal', 'high');
    add_meta_box('formato_metabox', 'Formato de Imagenes', 'show_formato_metabox', 'chicanu', 'side', 'default');

    // Metaboxes Chicos NU
//    add_meta_box('postid_metabox', 'Post ID', 'show_postid_metabox', 'chiconu', 'side', 'high');
    add_meta_box('galeria_metabox', 'Galeria de Fotos', 'show_galeria_metabox', 'chiconu', 'normal', 'high');
    add_meta_box('formato_metabox', 'Formato de Imagenes', 'show_formato_metabox', 'chiconu', 'side', 'default');

    // Metaboxes Banners
//    add_meta_box('postid_metabox', 'Post ID', 'show_postid_metabox', 'banner', 'side', 'high');
    add_meta_box('banner_metabox', 'Banner', 'show_banner_metabox', 'banner', 'normal', 'high', null);
    add_meta_box('link_mmetabox', 'Link URL', 'show_link_metabox', 'banner', 'normal', 'high');

    // Metaboxes Tapas
//    add_meta_box('postid_metabox', 'Post ID', 'show_postid_metabox', 'tapa', 'side', 'high');
    add_meta_box('link_metabox', 'Link URL', 'show_link_metabox', 'tapa', 'normal', 'high');

    // Metaboxes Videos
//	add_meta_box('postid_metabox', 'Post ID', 'show_postid_metabox', 'video', 'side', 'high');
	add_meta_box('video_metabox', 'Video URL', 'show_video_metabox', 'video', 'normal', 'high');


	//Disable Default Metaboxes from NOTICIA
    remove_meta_box('commentsdiv', 'noticia', 'normal');
    remove_meta_box('commentstatusdiv', 'noticia', 'normal');
}

?>