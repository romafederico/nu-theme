<?php

function show_banner_metabox()
{
    global $post;
    add_thickbox();
    wp_enqueue_script('media-upload');

    ?>
    <input type="hidden" name="banner_meta_box_nonce" value="<?= wp_create_nonce(basename(__FILE__)); ?>" />
    <script type="text/javascript">

    jQuery(function(jQuery) {
        jQuery('.banner_upload_button').click(function() {

            formfield = jQuery('.banner_meta_tag');
            preview = jQuery('.banner_preview');

            tb_show('', 'media-upload.php?post_id=<?= $post->ID; ?>&type=file&TB_iframe=true');

            window.send_to_editor = function(html) {

                var tag = '';

                if (jQuery(html).attr('href') && (jQuery(html).attr('href').indexOf('.swf')==-1) && (jQuery(html).attr('href').indexOf('.zip')==-1) ) {
                    tag = '<img src="'+jQuery(html).attr('href')+'" />';
                }

                else if(jQuery(html).attr('href') && (jQuery(html).attr('href').indexOf('.swf') > 0)) {
                    tag = "<OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0' ALIGN='' WIDTH='"+jQuery('#banner_meta_tag_width').val()+"' HEIGHT='"+jQuery('#banner_meta_tag_height').val()+"'><PARAM NAME='movie' VALUE='"+jQuery(html).attr('href')+"'> <PARAM NAME='quality' VALUE='high'> <EMBED src='"+jQuery(html).attr('href')+"' quality='high' ALIGN='' WIDTH='"+jQuery('#banner_meta_tag_width').val()+"' HEIGHT='"+jQuery('#banner_meta_tag_height').val()+"' TYPE='application/x-shockwave-flash' PLUGINSPAGE='http://www.macromedia.com/go/getflashplayer'></EMBED> </OBJECT>";
                }

                else if(jQuery(html).attr('href') && (jQuery(html).attr('href').indexOf('.zip')>0)) {

                    console.log('archivo a unzip ', jQuery(html).attr('href'));

                    var data = {
                        'action': 'zip_to_html',
                        'zippedfile': jQuery(html).attr('href')
                    };

                    jQuery.post(ajaxurl, data, function(response) {

                        console.log('link a index que vuelve ', response.link);

                        tag = "<iframe src='"+response.link+"' height='"+jQuery('#banner_meta_tag_height').val()+"' width='"+jQuery('#banner_meta_tag_width').val()+"' scrolling='no' frameborder='0' style='border:0px;'></iframe>";
                            formfield.val(tag);
                            preview.html(tag);
                            tb_remove();
                    });
                }

                formfield.val(tag);
                preview.html(tag);
                tb_remove();

            }
            return false;
        });
    });
    </script>
    <p class="description">Indique las medidas del banner solo para archivos .SWF o Banners HTML</p>
    <div><?php wp_upload_dir() ?> </div>
    <p>Ancho <input type="text" name="banner_meta_tag_width" id="banner_meta_tag_width" value="<?= get_post_meta($post->ID, 'banner_meta_tag_width', true); ?>"> >> Alto <input type="text" name="banner_meta_tag_height" id="banner_meta_tag_height" value="<?= get_post_meta($post->ID, 'banner_meta_tag_height', true); ?>" /> >> Subir/Insertar Archivo<input type="image" class="banner_upload_button" src="images/media-button.png?ver=20111005" /></p>
    <!-- <p>Subir/Insertar Archivo<input type="image" class="banner_upload_button" src="images/media-button.png?ver=20111005" /></p> -->
    <input type="hidden" class="banner_meta_tag" name="banner_meta_tag" value=' <?= get_post_meta($post->ID, 'banner_meta_tag', true); ?>'/>
    <div class="banner_preview"><?= get_post_meta($post->ID, 'banner_meta_tag', true); ?></div>
    <?php

}

function save_banner_metabox($post_id) {
    if (!isset($_POST['banner_meta_box_nonce']) || !wp_verify_nonce($_POST['banner_meta_box_nonce'], basename(__FILE__))):
        return $post_id;
    endif;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE):
        return $post_id;
    endif;

    if (!current_user_can('edit_post', $post_id)):
        return $post_id;
    endif;

    $old = get_post_meta($post_id, 'banner_meta_tag', true);
	$new = trim($_POST['banner_meta_tag']);

    if ($new && $new != $old):
        update_post_meta($post_id, 'banner_meta_tag', $new);
        update_post_meta($post_id, 'banner_meta_tag_width', trim($_POST['banner_meta_tag_width']));
        update_post_meta($post_id, 'banner_meta_tag_height', trim($_POST['banner_meta_tag_height'])); elseif ('' == $new && $old):
        delete_post_meta($post_id, 'banner_meta_tag');
        delete_post_meta($post_id, 'banner_meta_tag_width');
        delete_post_meta($post_id, 'banner_meta_tag_height');
    endif;
}

?>
