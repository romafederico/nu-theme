<?php

function show_copete_metabox()
{
    global $post;
    $copete = get_post_meta($post->ID, 'copete_meta_texto', true);
    wp_nonce_field('copete_metabox_nonce', 'meta_box_nonce');

	?>
	<textarea class="widefat" name="copete_meta_texto" id="copete_meta_texto"><?php echo $copete ?></textarea>
	<?php

}

function save_copete_metabox($post_id)
{
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    if (!isset($_POST['meta_box_nonce']) || !wp_verify_nonce($_POST['meta_box_nonce'], 'copete_metabox_nonce')) {
        return;
    }
    if (!current_user_can('edit_post')) {
        return;
    }
    if (isset($_POST['copete_meta_texto'])) {
        update_post_meta($post_id, 'copete_meta_texto', $_POST['copete_meta_texto']);
    }
}

?>
