<?php

function show_formato_metabox()
{
    global $post;
    $format = (get_post_format($post))?get_post_format($post):get_option('default_post_format');

    ?>
    <input type="hidden" name="destacada_meta_box_nonce" value="<?= wp_create_nonce(basename(__FILE__)); ?>" />
    <input type="radio" name="post_format" class="post-format" id="post-format-gallery" value="gallery" <?=checked($format, 'gallery'); ?> /> <label for="post-format-gallery">Imagen Grande/Galería</label><br />
    <input type="radio" name="post_format" class="post-format" id="post-format-image" value="image" <?=checked($format, 'image'); ?> /> <label for="post-format-image">Imagen Normal</label><br />
    <input type="radio" name="post_format" class="post-format" id="post-format-0" value="0" <?=checked($format, '0'); ?> /> <label for="post-format-0">Sin Imagen</label><br />
    <p class="description">Elija el formato con el que se mostraran la <strong>Imagen Destacada</strong> en el <strong>DETALLE DE LA NOTA</strong>, no afecta como se muestra en la <strong>HOME</strong> o en <strong>LISTADOS</strong>.</p>
    <script type="text/javascript">
        jQuery('#formatdiv').remove();
    </script>
    <?php

}

?>
