<?php

function show_galeria_metabox()
{
    global $post; ?>
    <input type="hidden" name="galeria_meta_box_nonce" value="<?= wp_create_nonce(basename(__FILE__)); ?>" />
    <script type="text/javascript">
    jQuery(function(jQuery) {
        jQuery('.galeria_upload_image_button').click(function() {
            formfield = jQuery(this).siblings('.galeria_upload_image');
            preview = jQuery(this).siblings('.galeria_preview_image');
            tb_show('', 'media-upload.php?type=image&TB_iframe=true');
            window.send_to_editor = function(html) {
                if(jQuery(html).attr('src')){
                    var imagen = jQuery(html);
                } else {
                    var imagen = jQuery('img', html);
                }
                src = imagen.attr('src');
                id = imagen.attr('class').replace(/(.*?)wp-image-/, '');
                formfield.val(id);
                preview.attr('src', src);
                tb_remove();
            }
            return false;
        });
        jQuery('.galeria_clear_image_button').click(function() {
            jQuery(this).parent().parent().find('.galeria_upload_image').val('');
            jQuery(this).parent().parent().find('.galeria_preview_image').attr('src', '');
            return false;
        });
        jQuery('.galeria_add_image_button').click(function() {
            last_item = jQuery('.galeria_imagenes li:last');
            item = last_item.clone(true);

            item.attr('style','');
            jQuery('img.galeria_preview_image', item).attr('src', '');
            jQuery('input.galeria_upload_image', item).val('').attr('name', function(index, name) {
                if(name=='galeria_meta_imagenes_mock'){
                    return 'galeria_meta_imagenes[0]';
                }
                return name.replace(/(\d+)/, function(fullMatch, n) {
                    return Number(n) + 1;
                });
            })
            item.insertAfter(last_item);
            return false;
        });
        jQuery('.galeria_remove_image_button').click(function(){
            jQuery(this).parent().remove();
            return false;
        });
        jQuery('.galeria_imagenes').sortable({
            opacity: 0.6,
            revert: true,
            cursor: 'move',
            handle: '.sort'
        });
    });
    </script>
    <ul class="galeria_imagenes">
        <li style="display:none; visibility: hidden;">
            <span class="sort hndle" style="float:left;"><img src="<?= get_bloginfo('template_url') . '/images/move.png'; ?>" />&nbsp;&nbsp;&nbsp;</span>
            <input name="galeria_meta_imagenes_mock" type="hidden" class="galeria_upload_image" value="" />
            <img src="" class="galeria_preview_image" alt="" width="200"/><br />
            <input class="galeria_upload_image_button button" type="button" value="Seleccionar Imagen" />
            <small><a href="#" class="galeria_clear_image_button">Eliminar Imagen</a></small>
            &nbsp;&nbsp;&nbsp;<a class="galeria_remove_image_button button" href="javascript:;">- Quitar Imagen</a>
        </li>
    <?php
        $meta = get_post_meta($post->ID, 'galeria_meta_imagenes', true);
        $i = 0;
        if ($meta):
        foreach ($meta as $row):
            $image = wp_get_attachment_image_src($row, 'full'); ?>
        <li>
            <span class="sort hndle" style="float:left;"><img src="<?= get_bloginfo('template_url') . '/images/move.png'; ?>" />&nbsp;&nbsp;&nbsp;</span>
            <input name="galeria_meta_imagenes[<?=$i; ?>]" type="hidden" class="galeria_upload_image" value="<?= $row; ?>" />
            <img src="<?= $image[0]; ?>" class="galeria_preview_image" alt="" width="200"/><br />
            <input class="galeria_upload_image_button button" type="button" value="Seleccionar Imagen" />
            <small><a href="#" class="galeria_clear_image_button">Eliminar Imagen</a></small>
            &nbsp;&nbsp;&nbsp;<a class="galeria_remove_image_button button" href="javascript:;">- Quitar Imagen</a>
        </li>
    <?php
        $i++;
        endforeach;
        endif;
    ?>
    </ul>
    <br />
    <a class="galeria_add_image_button button-primary" href="javascript:;">+ Agregar Imagen</a>
    <br clear="all" /><br /><p class="description">Agregue las imagenes que desee mostrar en la noticia, recuerde seleccionar el Formato de Imagen "Galería"</p>
    <?php
}

function save_galeria_metabox($post_id)
{
    if (!isset($_POST['galeria_meta_box_nonce']) || !wp_verify_nonce($_POST['galeria_meta_box_nonce'], basename(__FILE__))):
        return $post_id;
    endif;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE):
        return $post_id;
    endif;

    if (!current_user_can('edit_post', $post_id)):
        return $post_id;
    endif;

    $old = get_post_meta($post_id, 'galeria_meta_imagenes', true);
    $new = $_POST['galeria_meta_imagenes'];
    if ($new && $new != $old):
        update_post_meta($post_id, 'galeria_meta_imagenes', $new); elseif ('' == $new && $old):
        delete_post_meta($post_id, 'galeria_meta_imagenes', $old);
    endif;
}

?>