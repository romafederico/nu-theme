<?php

function show_link_metabox()
{
    global $post;
    ?>
        <input type="hidden" name="link_meta_box_nonce" value="<?= wp_create_nonce(basename(__FILE__)); ?>" />
        <textarea name="link_meta_url" class="widefat" ><?= get_post_meta($post->ID, 'link_meta_url', true); ?></textarea><br/>
        <p class="description">Agregue la URL de la tapa en PDF.</p>
    <?php

}

function save_link_metabox($post_id)
{
    if (!isset($_POST['link_meta_box_nonce']) || !wp_verify_nonce($_POST['link_meta_box_nonce'], basename(__FILE__))):
        return $post_id;
    endif;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE):
        return $post_id;
    endif;

    if (!current_user_can('edit_post', $post_id)):
        return $post_id;
    endif;

    // $old = get_post_meta($post_id, 'link_meta_url', true);
    // $new = trim($_POST['link_meta_url']);
    // if ($new && $new != $old):
    //     update_post_meta($post_id, 'link_meta_url', $new); elseif ('' == $new && $old):
    //     delete_post_meta($post_id, 'link_meta_url', $old);
    // endif;

    if (isset($_POST['link_meta_url'])) {
        update_post_meta($post_id, 'link_meta_url', $_POST['link_meta_url']);
    }
}

?>
