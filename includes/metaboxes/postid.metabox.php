<?php

function show_postid_metabox()
{
    global $post;
    $postId = get_the_id();

    ?>
    <p class="description">Copiar este ID para incluir en el Widget correspondiente</p>
    <h3><?php echo $postId ?></h3>
    <?php

}
