<?php

function show_redaccion_metabox()
{
    global $post;
    $redaccion_meta_opcion = get_post_meta($post->ID, 'redaccion_meta_opcion', true);
    if (!$redaccion_meta_opcion):
        $redaccion_meta_opcion = "autor";
    endif;

    ?>
    <input type="hidden" name="redaccion_meta_box_nonce" value="<?= wp_create_nonce(basename(__FILE__)); ?>" />
    <p class="description">El Articulo se mostrará como escrito por:</p>
    <label><input name="redaccion_meta_opcion" type="radio" value="autor" <?=checked($redaccion_meta_opcion, "autor"); ?> /> <?=get_the_author_meta('display_name', $post->post_author); ?></label><br/>
    <label><input name="redaccion_meta_opcion" type="radio" value="redaccion" <?=checked($redaccion_meta_opcion, "redaccion"); ?> /> Redacción Noticias Urbanas</label>
    <?php

}

function save_redaccion_metabox($post_id)
{
    if (!isset($_POST['redaccion_meta_box_nonce']) || !wp_verify_nonce($_POST['redaccion_meta_box_nonce'], basename(__FILE__))):
        return $post_id;
    endif;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE):
        return $post_id;
    endif;

    if (!current_user_can('edit_post', $post_id)):
        return $post_id;
    endif;

    $old = get_post_meta($post_id, 'redaccion_meta_opcion', true);
    $new = trim($_POST['redaccion_meta_opcion']);
    if ($new && $new != $old):
        update_post_meta($post_id, 'redaccion_meta_opcion', $new); elseif ('' == $new && $old):
        delete_post_meta($post_id, 'redaccion_meta_opcion', $old);
    endif;
}

 ?>
