<?php

function show_subtitulo_metabox()
{
    global $post;
?>
    <input type="hidden" name="subtitulo_meta_box_nonce" value="<?= wp_create_nonce(basename(__FILE__)); ?>" />
    <textarea id="subtitulo_meta_texto" name="subtitulo_meta_texto" style="width:100%" rows="5" maxlength="200" oninput="charsleft(jQuery(this),200);"><?= get_post_meta($post->ID, 'subtitulo_meta_texto', true); ?></textarea><br/>
    <p class="description" id="subtitulo_meta_texto_chars"></p>
    <script type="text/javascript">

    function charsleft(textarea,limit){
        var quedan = (limit - textarea.val().length);
        if(quedan == limit) {
            jQuery('#'+textarea.attr('name')+'_chars').html('Máximo '+ limit + ' caracteres.');
        } else {
            jQuery('#'+textarea.attr('name')+'_chars').html('Quedan '+ quedan + ' caracteres.');
        }
    }
    charsleft(jQuery('#subtitulo_meta_texto'),200);
    </script>
<?php
}

function save_subtitulo_metabox($post_id)
{
    if (!isset($_POST['subtitulo_meta_box_nonce']) || !wp_verify_nonce($_POST['subtitulo_meta_box_nonce'], basename(__FILE__))):
        return $post_id;
    endif;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE):
        return $post_id;
    endif;

    if (!current_user_can('edit_post', $post_id)):
        return $post_id;
    endif;

    // $old = get_post_meta($post_id, 'subtitulo_meta_texto', true);
    // $new = trim($_POST['subtitulo_meta_texto']);
    // $new = (strlen($subtitulo)>200)?substr($new, 0, 200):$new;

    if(isset($_POST['subtitulo_meta_texto'])) {
        update_post_meta($post_id, 'subtitulo_meta_texto', $_POST['subtitulo_meta_texto']);
    }

    // if ($new && $new != $old):
    //     update_post_meta($post_id, 'subtitulo_meta_texto', $new); elseif ('' == $new && $old):
    //     delete_post_meta($post_id, 'subtitulo_meta_texto', $old);
    // endif;
}

?>
