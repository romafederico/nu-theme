<?php

function show_video_metabox() {
	global $post;
	?>
	<input type="hidden" name="video_meta_box_nonce" value="<?= wp_create_nonce(basename(__FILE__)); ?>" />
	<textarea name="video_meta_url" cols="150" rows="10"><?= get_post_meta($post->ID, 'video_meta_url', true); ?></textarea><br/>
	<p class="description">Agregue la URL completa del video (YouTube, Vimeo, etc) que desea embeber.</p>
	<?php
}

function save_video_metabox($post_id) {
	if (!isset($_POST['video_meta_box_nonce']) || !wp_verify_nonce($_POST['video_meta_box_nonce'], basename(__FILE__))):
		return $post_id;
	endif;

	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE):
		return $post_id;
	endif;

	if (!current_user_can('edit_post', $post_id)):
		return $post_id;
	endif;

	$old = get_post_meta($post_id, 'video_meta_url', true);
	$new = trim($_POST['video_meta_url']);

	if ($new && $new != $old):
		update_post_meta($post_id, 'video_meta_url', $new);
	elseif ('' == $new && $old):
		delete_post_meta($post_id, 'video_meta_url', $old);
	endif;
}