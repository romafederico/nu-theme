<?php

// Enable Default Post Types
function configure_post_types()
{

    // Post type = NOTICIA
    register_post_type('noticia', array(
            'labels' => array(
                'name' => 'Noticias',
                'singular_name' => 'Noticia',
                'all_items' => 'Todas las Noticias'
            ),
            'public' => true,
            'has_archive' => true,
            'menu_position' => 5,
            'rewrite' => array(
                'slug' => 'noticias'
            ),
            'query_var' => 'noticia',
            'supports' => array(
                'title',
                'editor',
                'post-formats',
                'thumbnail'
            ),
            'taxonomies' => array(
                'category',
                'post_tag'
            )
        )
    );

    // Post type = OPINION
    register_post_type('opinion', array(
            'labels' => array(
                'name' => 'Opinión',
                'singular_name' => 'Nota de Opinión',
                'all_items' => 'Todas las Notas de Opinión'
            ),
            'public' => true,
            'has_archive' => true,
            'menu_position' => 10,
            'rewrite' => array(
                'slug' => 'opinion'
            ),
            'query_var' => 'opinion',
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            ),
            'taxonomies' => array(
                'category',
                'post_tag'
            )
        )
    );

    // Post type = PASILLO
    register_post_type('pasillo', array(
            'labels' => array(
                'name' => 'Pasillo',
                'singular_name' => 'Pasillo Roverano',
                'all_items' => 'Todos en Pasillo Roverano'
            ),
            'public' => true,
            'has_archive' => true,
            'menu_position' => 10,
            'rewrite' => array(
                'slug' => 'pasillo'
            ),
            'query_var' => 'pasillo',
            'supports' => array(
                'title',
                'editor',
                'post-formats',
                'thumbnail',
            ),
            'taxonomies' => array(
                'category',
                'post_tag'
            )
        )
    );

    // Post type = BANNER
    register_post_type('banner', array(
            'labels' => array(
                'name' => 'Banners',
                'singular_name' => 'Banner',
                'all_items' => 'Todos los Banners'
            ),
            'public' => true,
            'has_archive' => false,
            'menu_position' => 10,
            'supports' => array(
                'title'
            )
        )
    );

    // Post type = CHICA NU
    register_post_type('chicanu', array(
        'labels' => array(
            'name' => 'Chicas NU',
            'singular_name' => 'Chica NU',
            'all_items' => 'Todas las Chicas NU'
        ),
            'public' => true,
            'has_archive' => true,
            'menu_position' => 10,
            'rewrite' => array(
                'slug' => 'chicas-nu'
            ),
            'query_var' => 'chica-nu',
            'supports' => array(
                'title',
                'post-formats',
                'thumbnail'
            ),
            'taxonomies' => array(
                'post_tag'
            )
        )
    );

    register_post_type('chiconu', array(
        'labels' => array(
            'name' => 'Chicos NU',
            'singular_name' => 'Chico NU',
            'all_items' => 'Todos los Chicos NU'
        ),
            'public' => true,
            'has_archive' => true,
            'menu_position' => 10,
            'rewrite' => array(
                'slug' => 'chicos-nu'
            ),
            'query_var' => 'chico-nu',
            'supports' => array(
                'title',
                'post-formats',
                'thumbnail'
            ),
            'taxonomies' => array(
                'post_tag'
            )
        )
    );

    register_post_type('tapa', array(
        'labels' => array(
            'name' => 'Tapas',
            'singular_name' => 'Tapa',
            'all_items' => 'Todas las Tapas'
        ),
            'public' => true,
            'has_archive' => true,
            'menu_position' => 10,
            'exclude_from_search' => true,
            'supports' => array(
                'title',
                'thumbnail'
            )
        )
    );

    register_post_type('video', array(
        'labels' => array(
            'name' => 'Videos',
            'singular_name' => 'Video',
            'all_items' => 'Todos los Videos'
        ),
            'public' => true,
            'has_archive' => false,
            'menu_position' => 10,
            'exclude_from_search' => true,
            'supports' => array(
                'title'
            )
        )
    );
}
