<?php
$settings = 'nu-options';

$defaults = array(
    'social_twitter' => 1,
    'social_twitter_url' => '',
    'social_facebook' => 1,
    'social_facebook_url' => '',
    'social_youtube' => 1,
    'social_youtube_url' => '',
    'social_rss' => 1,
    'social_rss_url' => get_bloginfo('url').'/feed/rss/',
    'cant_posts_destacadas' => 15,
    'cant_posts_secciones' => 15,
    'cant_posts_busquedas' => 15,
    'agora' => '',
    'agora_limit' => 15,
    'tag_ids' => '',
    'redaccion' => 'Redacción Noticias Urbanas',
    'tracking' => '<!--COLOCAR EL SCRIPT DE ANALYTICS AQUI-->'
);

//guardo las opciones por default si todavia no existen
add_option($settings, $defaults, '', 'yes');

add_action('admin_init', 'theme_options_init');

function theme_options_init() {
    global $settings;
    register_setting($settings, $settings);
}

add_action('admin_menu', 'theme_options_add_page');

function theme_options_add_page() {
    add_theme_page("Opciones del Tema", "Opciones del Tema", 'edit_theme_options', 'theme_options', 'theme_options_page');
}

function theme_options_page() {
    wp_enqueue_script('jquery-ui-sortable');
?>
    <style type="text/css">
        .metabox-holder {
            float: left;
            margin: 0; padding: 0 10px 0 0;
        }
        .metabox-holder {
            float: left;
            margin: 0; padding: 0 10px 0 0;
        }
        .metabox-holder .postbox .inside {
            padding: 0 10px;
        }
        .mbleft {
            width: 300px;
        }
        .mbright {
            width: 480px;
        }
        .catchecklist,
        .pagechecklist {
            list-style-type: none;
            margin: 0; padding: 0 0 10px 0;
        }
        .catchecklist li,
        .pagechecklist li {
            margin: 0; padding: 0;
        }
        .catchecklist ul {
            margin: 0; padding: 0 0 0 15px;
        }
        select {
            margin-top: 5px;
        }
        input {
            margin-top: 5px;
        }
        input[type="checkbox"], input[type="radio"] {
            margin-top: 1px;
        }
    </style>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            jQuery(".fade").fadeIn(1000).fadeTo(1000, 1).fadeOut(1000);
        });
    </script>

    <div class="wrap">
        <?php
        // display the proper notification if Saved/Reset
        global $settings, $defaults;

        if (theme_options('reset')) {
            echo '<div class="updated fade" id="message"><p>Opciones del Tema <strong>RESETEADAS</strong></p></div>';
            update_option($settings, $defaults);
        } elseif (isset($_REQUEST['settings-updated'])&& $_REQUEST['settings-updated']) {
            echo '<div class="updated fade" id="message"><p>Opciones del Tema <strong>GUARDADAS</strong></p></div>';
        }

        ?>
        <h2><?= wp_get_theme(); ?> - Opciones</h2>
        <form method="post" action="options.php">
            <?php settings_fields($settings); // important!  ?>

          <!--right column-->
            <div class="metabox-holder mbright">

                <div class="postbox">
                    <h3>Barra de Tags</h3>
                    <div class="inside">
                        <p>Selecciona los tags a mostrar en el widget y en las paginas interiores.</p>
                        <script type="text/javascript">
                            jQuery(function(jQuery) {
                                jQuery('.taglist_add_button').click(function() {
                                    if(jQuery('.input_tag_id').val()!=""){
                                        last_item = jQuery('.taglist li:last');
                                        item = last_item.clone(true);
                                        item.attr('style','');
                                        jQuery('label.taglist_tag_name', item).html(jQuery('.input_tag_name').val());
                                        jQuery('input.taglist_tag_id', item).val(jQuery('.input_tag_id').val()).attr('name', function(index, name) {
                                            if(name=='tag_ids_mock'){
                                                return '<?= $settings; ?>[tag_ids][0]';
                                            }
                                            return name.replace(/(\d+)/, function(fullMatch, n) {
                                                return Number(n) + 1;
                                            });
                                        })
                                        item.insertAfter(last_item);
                                        jQuery('.input_tag_name').autocomplete('option','source',getAutocompleteTagListSource());
                                    }
                                    return false;
                                });
                                jQuery('.taglist_remove_button').click(function(){
                                    jQuery(this).parent().remove();
                                    jQuery('.input_tag_name').autocomplete('option','source',getAutocompleteTagListSource());
                                    return false;
                                });
                                jQuery('.taglist').sortable({
                                    opacity: 0.6,
                                    revert: true,
                                    cursor: 'move',
                                    handle: '.sort'
                                });
                                jQuery('.input_tag_name').autocomplete({
                                    source: getAutocompleteTagListSource(),
                                    minLength: 3,
                                    select: function( event, ui ) {
                                        jQuery('.input_tag_id').val(ui.item.id);
                                    }
                                });
                                function getAutocompleteTagListSource(){
                                    var source = ajaxurl+'?action=autocomplete_taglist';
                                    source = source + '&exclude=' + jQuery.map(jQuery('.taglist_tag_id'),function(i){ if(i.value!='') return i.value; }).join('-');
                                    return source;
                                }
                            });
                        </script>

                        <label>Buscar Tags: <input type="text" class="input_tag_name" autocomplete="off" style="width: 65%;" /><input type="hidden" class="input_tag_id" value="" /></label>
                        <a class="taglist_add_button button-primary" href="javascript:;">+ Agregar</a>
                        <ul class="taglist ui-sortable">
                            <li style="display:none; visibility: hidden;">
                                <span class="sort hndle" style="float:left;"><img src="<?= get_bloginfo('template_url') . '/images/move.png'; ?>" />&nbsp;&nbsp;&nbsp;</span>
                                <input name="tag_ids_mock" type="hidden" class="taglist_tag_id" value="" />
                                <label class="taglist_tag_name"></label>
                                &nbsp;&nbsp;&nbsp;<a class="taglist_remove_button button" href="javascript:;">- Quitar</a>
                            </li>
                            <?php
                            $tag_ids = theme_options('tag_ids');
                            $i = 0;
                            if ($tag_ids):
                                foreach ($tag_ids as $tag_id):
                                    $tag = get_tag($tag_id);
                                    ?>
                                    <li>
                                        <span class="sort hndle" style="float:left;"><img src="<?= get_bloginfo('template_url') . '/images/move.png'; ?>" />&nbsp;&nbsp;&nbsp;</span>
                                        <input name="<?= $settings; ?>[tag_ids][<?= $i; ?>]" type="hidden" class="taglist_tag_id" value="<?=$tag_id;?>" />
                                        <label class="taglist_tag_name"><?= $tag->name; ?></label>
                                        &nbsp;&nbsp;&nbsp;<a class="taglist_remove_button button" href="javascript:;">- Quitar</a>
                                    </li>
                                    <?php $i++;
                                endforeach;
                            endif;
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="postbox">
                    <h3>Redacción</h3>
                    <div class="inside">
                        <p>Especifica lo que deberia ser mostrado en vez del nombre del autor cuando sea requerido:</p>
                        <p><input class="widefat" type="text" name="<?= $settings; ?>[redaccion]" value="<?= theme_options('redaccion'); ?>" size="45" /></p>
                    </div>
                </div>

                <div class="postbox">
                    <h3>Tracking</h3>
                    <div class="inside">
                        <p>Pegar al final Google Analytics tracking script u equivalente. Es posible agregar todos los tracking scripts que sean necesarios.</p>
                        <p><textarea class="widefat" name="<?= $settings; ?>[tracking]" cols=85 rows=5><?= stripslashes(theme_options('tracking')); ?></textarea></p>
                    </div>
                </div>

                <p class="submit" style="text-align: right">
                  <input type="submit" class="button-primary" value="Guardar Opciones" />
                  <!-- <input type="submit" class="button-highlighted" name="<?= $settings; ?>[reset]" value="Resetear Opciones" /> -->
                </p>
            </div>
      <!--end right column-->
        </form>

    </div><!--end .wrap-->
<?php
}
