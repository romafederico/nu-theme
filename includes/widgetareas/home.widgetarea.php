<?php

register_sidebar(array(
	'name' => 'Home - Página principal',
	'id' => 'widgets-home',
	'before_widget' => '<div class="container">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>'
	)
);

?>
