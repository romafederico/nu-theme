<?php

// Include Custom Widget Areas
include(TEMPLATEPATH . "/includes/widgetareas/home.widgetarea.php");
include(TEMPLATEPATH . "/includes/widgetareas/sidebar.widgetarea.php");

// Include Custom Widgets
include(TEMPLATEPATH . "/includes/widgets/home.mosaico.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.tresnoticias.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.dosnoticias.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.dosnoticias.banner_r.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.dosnoticias.banner_l.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.noticia.video.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.noticia.imagen.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.noticia.padron.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.noticia.pasillo.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.secundarias.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.chicxs.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.opinion.widget.php");
//include(TEMPLATEPATH . "/includes/widgets/home.banner.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.dosbanner.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.portada.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.portadainteractiva.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.portadaunica.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.tags.widget.php");
include(TEMPLATEPATH . "/includes/widgets/home.ticker.widget.php");
include(TEMPLATEPATH . "/includes/widgets/sidebar.banner.widget.php");

// Register Custom Widgets
function register_all_widgets()
{
    register_widget('home_mosaico');
    register_widget('tres_noticias');
    register_widget('dos_noticias');
    register_widget('dos_noticias_banner_r');
    register_widget('dos_noticias_banner_l');
    register_widget('noticias_secundarias');
    register_widget('noticia_video');
    register_widget('noticia_imagen');
    register_widget('noticia_padron');
    register_widget('noticia_pasillo');
    register_widget('chicxs');
    register_widget('home_opinion');
//    register_widget('banner');
    register_widget('banner_sidebar');
    register_widget('dos_banner');
    register_widget('home_portada');
    register_widget('home_portada_unica');
    register_widget('home_portada_interactiva');
    register_widget('home_portada_unica');
    register_widget('home_tags');
    register_widget('home_ticker');

    // Disable Default WP Widgets
    unregister_widget('WP_Widget_Pages');
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Search');
    unregister_widget('WP_Widget_Text');
    unregister_widget('WP_Widget_Categories');
    unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Tag_Cloud');
    unregister_widget('WP_Nav_Menu_Widget');
    unregister_widget('WP_Widget_Polls');
    unregister_widget('WP_Widget_PostRatings');
    unregister_widget('WP_Widget_PostViews');
    unregister_widget('MinitwitterWidget');
	unregister_widget('WP_Widget_Media_Audio' );
	unregister_widget('WP_Widget_Media_Image');
	unregister_widget('WP_Widget_Media_Video');
}

function auto_complete($post_type = 'noticia', $field_id, $field_name, $field_class, $selected_id = '', $selected_title = ''){
    ?>
    <script type="text/javascript">
        jQuery(function(jQuery) {

            if(jQuery('#<?=$field_id;?>_autocomplete_id').val()>0){
                jQuery('#<?=$field_id;?>_autocomplete').hide();
                jQuery('#<?=$field_id;?>_autocomplete_title').show();
                jQuery('#<?=$field_id;?>_autocomplete_cambiar').show();
                jQuery('#<?=$field_id;?>_autocomplete_cambiar').click(function() {
                    jQuery('#<?=$field_id;?>_autocomplete_title').hide();
                    jQuery('#<?=$field_id;?>_autocomplete_cambiar').hide();
                    jQuery('#<?=$field_id;?>_autocomplete_id').val('');
                    jQuery('#<?=$field_id;?>_autocomplete').val('');
                    jQuery('#<?=$field_id;?>_autocomplete').show();
                });
            }else{
                jQuery('#<?=$field_id;?>_autocomplete_title').hide();
                jQuery('#<?=$field_id;?>_autocomplete_cambiar').hide();
                jQuery('#<?=$field_id;?>_autocomplete').show();
            }

        });
    </script>
    <input type="hidden" name="<?=$field_name;?>" class="<?=$field_class;?>" value="<?=$selected_id;?>" id="<?=$field_id;?>_autocomplete_id"/>
    <script type="text/javascript">
        //console.log(jQuery(this).closest('.widgets-sortables')[0].id);
        //console.log(jQuery(this).closest('.widgets-sortables')[0]);

        //control-section
        function getParent(_this){
            var closer = jQuery(_this).closest('.widgets-sortables')
            if(closer.length==0){
                closer = jQuery(_this).closest('.control-section')
            }
            return closer[0].id;
        }
    </script>
    <input
        type="text"
        id="<?=$field_id;?>_autocomplete"
        class="autocomplete_widget widefat"
        autocomplete="off"
        value=""
        onclick="autocomplete_widget(this.id,'<?=$post_type;?>','.'+getParent(this)+'_autocomplete');"
    />
    <h4 id="<?=$field_id;?>_autocomplete_title" style="float:left;margin:0px;"><?=$selected_title;?></h4>&nbsp;<a href="javascript:;" id="<?=$field_id;?>_autocomplete_cambiar">(cambiar)</a>
    <?php
}

?>
