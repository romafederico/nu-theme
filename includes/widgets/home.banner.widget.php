<?php

class banner extends WP_Widget {

	function __construct() {
		parent::__construct('banner', 'Banner Billboard', array('description' => __('Formatos posibles: .jpg, .gif, .html'),
			'banner' => '',
			'banner_mobile' => '',
			)
		);
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['banner'] = strip_tags($new_instance['banner']);
		$instance['banner_mobile'] = strip_tags($new_instance['banner_mobile']);
		return $instance;
	}


	// Backend part of the widget
	public function form($instance) {

		if($instance) {
			$banner = esc_attr($instance['banner']);
			$banner_mobile = esc_attr($instance['banner_mobile']);
		} else {

		}

		?>
            <h2>Banner Billboard</h2>
			<p>
                <h4>Tamaño Desktop: 970px de ancho x 250px de alto</h4>
				<label for="<?php echo $this->get_field_id('banner');?>">Seleccione Banner</label>
				<input class="widefat"
				id="<?php echo $this->get_field_id('banner'); ?>"
				name="<?php echo $this->get_field_name('banner'); ?>"
				type="number"
				value="<?php echo $banner; ?>" />
			</p>

            <p>
                <h4>Tamaño Mobile: 300px de ancho x 250px de alto</h4>
				<label for="<?php echo $this->get_field_id('banner_mobile');?>">Seleccione Banner</label>
				<input class="widefat"
				id="<?php echo $this->get_field_id('banner_mobile'); ?>"
				name="<?php echo $this->get_field_name('banner_mobile'); ?>"
				type="number"
				value="<?php echo $banner_mobile; ?>" />
			</p>

		<?php
	}

	// Frontend part of the widget
	function widget($args, $instance) {
		$banner = apply_filters('banner', $instance['banner']);
		$banner_link = get_post_meta($banner, 'link_meta_url', true);
		$banner_img = get_post_meta($banner, 'banner_meta_tag', true);

		$banner_mobile = apply_filters('banner', $instance['banner_mobile']);
		$banner_mobile_link = get_post_meta($banner_mobile, 'link_meta_url', true);
		$banner_mobile_img = get_post_meta($banner_mobile, 'banner_meta_tag', true);

		echo '<div class="row">';
		echo '<div class="container">';
		echo '<div class="col-md-12 banner-billboard banner-center hidden-xs hidden-sm"><a href="' . $banner_link . '" target="_blank">'.$banner_img.'</a></div>';
		echo '<div class="col-md-12 banner-billboard banner-center visible-xs visible-sm"><a href="' . $banner_mobile_link . '" target="_blank">'.$banner_mobile_img.'</a></div>';
		echo '</div>';
		echo '</div>';
	}
}

?>
