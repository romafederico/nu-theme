<?php

class chicxs extends WP_Widget {

	function __construct() {
		parent::__construct('chicxs', 'Chicxs NU y Tapas', array('description' => __('Chica NU y Chico NU a la izquierda y Tapas a la derecha'),
			'chica' => '',
			'chico' => '',

			)
		);
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['chica'] = strip_tags($new_instance['chica']);
		$instance['chico'] = strip_tags($new_instance['chico']);
		return $instance;
	}


	// Backend part of the widget
	public function form($instance) {
		if($instance) {
			$chica = esc_attr($instance['chica']);
			$chico = esc_attr($instance['chico']);
		} else {

		}

		?>
			<p>
				<label for="<?php echo $this->get_field_id('chica');?>">Chica NU</label>
                <?= auto_complete('chicanu', $this->get_field_id('chica'), $this->get_field_name('chica'), $chica.'_autocomplete', $chica, get_post_field('post_title',$chica)); ?>

            </p>
            <p>
                <label for="<?php echo $this->get_field_id('chico');?>">Chico NU</label>
                <?= auto_complete('chiconu', $this->get_field_id('chico'), $this->get_field_name('chico'), $chico.'_autocomplete', $chico, get_post_field('post_title',$chico)); ?>

            </p>

		<?php
	}

	// Frontend part of the widget
	function widget($args, $instance) {
		$chica = apply_filters('chica', $instance['chica']);
		$chico = apply_filters('chico', $instance['chico']);
		?>

		<div class="row">
			<div class="container">

                <script type="text/javascript">
                    $(document).ready(function() {
                        $("#lightSlider-Chicxs").lightSlider({
                            item: 1,
                            autoWidth: false,
                            slideMove: 1, // slidemove will be 1 if loop is true
                            slideMargin: 10,

                            addClass: '',
                            mode: "slide",
                            useCSS: true,
                            cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                            easing: 'linear', //'for jquery animation',////

                            speed: 400, //ms'
                            auto: true,
                            loop: true,
                            slideEndAnimation: true,
                            pause: 9000,

                            keyPress: true,
                            controls: true,
                            prevHtml: '<img style= "margin-left:20px;" height="35px" src="<?php bloginfo('stylesheet_directory');?>/images/arrow-prev.svg" />',
                            nextHtml: '<img style= "margin:-20px;" height="35px" src="<?php bloginfo('stylesheet_directory');?>/images/arrow-next.svg" />',

                            rtl:false,
                            adaptiveHeight:false,

                            vertical:false,
                            verticalHeight:500,
                            vThumbWidth:100,

                            thumbItem:10,
                            pager: false,
                            gallery: false,
                            galleryMargin: 5,
                            thumbMargin: 5,
                            currentPagerPosition: 'middle',

                            enableTouch:true,
                            enableDrag:false,
                            freeMove:true,
                            swipeThreshold: 40,

                            responsive : [],

                            onBeforeStart: function (el) {},
                            onSliderLoad: function (el) {},
                            onBeforeSlide: function (el) {},
                            onAfterSlide: function (el) {},
                            onBeforeNextSlide: function (el) {},
                            onBeforePrevSlide: function (el) {}
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $("#lightSlider-Tapas").lightSlider({
                            item: 1,
                            autoWidth: false,
                            slideMove: 1, // slidemove will be 1 if loop is true
                            slideMargin: 5,

                            addClass: '',
                            mode: "slide",
                            useCSS: true,
                            cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                            easing: 'linear', //'for jquery animation',////

                            speed: 400, //ms'
                            auto: false,
                            loop: true,
                            slideEndAnimation: true,
                            pause: 9000,

                            keyPress: true,
                            controls: true,
                            prevHtml: '<img style= "margin-left:20px;" height="35px" src="<?php bloginfo('stylesheet_directory');?>/images/arrow-prev.svg" />',
                            nextHtml: '<img style= "margin:-20px;" height="35px" src="<?php bloginfo('stylesheet_directory');?>/images/arrow-next.svg" />',

                            rtl:false,
                            adaptiveHeight:false,

                            vertical:false,
                            verticalHeight:500,
                            vThumbWidth:100,

                            thumbItem:5,
                            pager: false,
                            gallery: false,
                            galleryMargin: 5,
                            thumbMargin: 5,
                            currentPagerPosition: 'middle',

                            enableTouch:true,
                            enableDrag:false,
                            freeMove:true,
                            swipeThreshold: 40,

                            responsive : [],

                            onBeforeStart: function (el) {},
                            onSliderLoad: function (el) {},
                            onBeforeSlide: function (el) {},
                            onAfterSlide: function (el) {},
                            onBeforeNextSlide: function (el) {},
                            onBeforePrevSlide: function (el) {}
                        });
                    });
                </script>

                <div class="nota-slider col-md-6 gutter-sm">
                    <ul id="lightSlider-Chicxs">
                        <li>
                            <a href="<?php echo get_post_permalink($chica) ?>">
                                <div class="hidden-xs hidden-sm"><img src="<?php echo get_the_post_thumbnail_url($chica,'chicxs') ?>" width="100%"></div>
                                <div class="visible-xs visible-sm"><img src="<?php echo get_the_post_thumbnail_url($chica,'col6') ?>" width="100%"></div>
                                <div class="chicxs-itembox">
                                    <h3><?php echo get_the_title($chica) ?></h3>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo get_post_permalink($chico) ?>">
                                <div class="hidden-xs hidden-sm"><img src="<?php echo get_the_post_thumbnail_url($chico,'chicxs') ?>" width="100%"></div>
                                <div class="visible-xs visible-sm"><img src="<?php echo get_the_post_thumbnail_url($chico,'col6') ?>" width="100%"></div>
                                <div class="chicxs-itembox">
                                    <h3><?php echo get_the_title($chico) ?></h3>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-6 gutter-sm">
                    <ul id="lightSlider-Tapas">
                    <?php
                    $args = array(
	                    'posts_per_page' => 5,
	                    'post_type' => 'tapa',
	                    'orderby' => 'date',
	                    'order' => 'DESC',
	                    'post_status' => 'publish'
                    );

                    $query = new WP_Query($args);
                     if($query->have_posts()) :
	                    while($query->have_posts()) : $query->the_post();
                        ?>
                            <div class="tapa-item">
                                <li><a href="<?php echo get_post_meta(get_the_ID(), 'link_meta_url', true); ?>" target="_blank"><img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'tapa'); ?>"><h4><?php echo get_the_title(); ?></h4></a></li>
                            </div>

                        <?php

	                    endwhile;
	                    wp_reset_postdata();
                    endif;

                    ?>
                    </ul>
                </div>
			</div>
		</div>
		<?php
	}
}

?>
