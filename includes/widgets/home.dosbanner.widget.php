<?php

class dos_banner extends WP_Widget {

	function __construct() {
		parent::__construct('dos_banner', '2 Banners', array('description' => __('Formatos posibles: .jpg, .gif, .html'),
			'banner_L' => '',
			'banner_L_mobile' => '',
			'banner_R' => ''
			)
		);
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['banner_L'] = strip_tags($new_instance['banner_L']);
		$instance['banner_L_mobile'] = strip_tags($new_instance['banner_L_mobile']);
		$instance['banner_R'] = strip_tags($new_instance['banner_R']);
		return $instance;
	}


	// Backend part of the widget
	public function form($instance) {

		if($instance) {
			$banner_L = esc_attr($instance['banner_L']);
			$banner_L_mobile = esc_attr($instance['banner_L_mobile']);
			$banner_R = esc_attr($instance['banner_R']);
		} else {

		}

		?>
            <h2>Banner Izquierdo</h2>

        <p>
                <h4>Tamaño Desktop: 728px de ancho x 210px de alto</h4>
				<label for="<?php echo $this->get_field_id('banner_L');?>">Seleccione Banner</label>
                <?= auto_complete('banner', $this->get_field_id('banner_L'), $this->get_field_name('banner_L'), $banner_L.'_autocomplete', $banner_L, get_post_field('post_title',$banner_L)); ?>

        </p>

			<p>
                <h4>Tamaño Mobile: 300px de ancho x 250px de alto</h4>
				<label for="<?php echo $this->get_field_id('banner_L_mobile');?>">Seleccione Banner</label>
                <?= auto_complete('banner', $this->get_field_id('banner_L_mobile'), $this->get_field_name('banner_L_mobile'), $banner_L_mobile.'_autocomplete', $banner_L_mobile, get_post_field('post_title',$banner_L_mobile)); ?>
			</p>

            <h2>Banner Derecho</h2>
			<p>
                <h4>Tamaño único: 300px de ancho x 250px de alto</h4>
				<label for="<?php echo $this->get_field_id('banner_R');?>">Seleccione Banner</label>
                <?= auto_complete('banner', $this->get_field_id('banner_R'), $this->get_field_name('banner_R'), $banner_R.'_autocomplete', $banner_R, get_post_field('post_title',$banner_R)); ?>
        </p>

		<?php
	}

	// Frontend part of the widget
	function widget($args, $instance) {
		$banner_L = apply_filters('banner_L', $instance['banner_L']);
		$banner_L_mobile = apply_filters('banner_L_mobile', $instance['banner_L_mobile']);
		$banner_R = apply_filters('banner_R', $instance['banner_R']);
		$banner_L_link = get_post_meta($banner_L, 'link_meta_url', true);
		$banner_L_mobile_link = get_post_meta($banner_L_mobile, 'link_meta_url', true);
		$banner_R_link = get_post_meta($banner_R, 'link_meta_url', true);
		$banner_L_img = get_post_meta($banner_L, 'banner_meta_tag', true);
		$banner_L_mobile_img = get_post_meta($banner_L_mobile, 'banner_meta_tag', true);
		$banner_R_img = get_post_meta($banner_R, 'banner_meta_tag', true);

		echo '<div class="row">';
		echo '<div class="container">';
		echo '<div class="col-md-8 gutter-sm hidden-xs hidden-sm banner-dos banner-center"><a href="' . $banner_L_link . '" target="_blank">'.$banner_L_img.'</a></div>';
		echo '<div class="col-md-8 gutter-sm visible-xs visible-sm banner-dos banner-center"><a href="' . $banner_L_mobile_link . '" target="_blank">'.$banner_L_mobile_img.'</a></div>';
		echo '<div class="col-md-4 gutter-sm banner-dos banner-center"><a href="' . $banner_R_link . '" target="_blank">'.$banner_R_img.'</a></div>';
		echo '</div>';
		echo '</div>';
	}
}

?>
