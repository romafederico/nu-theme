<?php

class dos_noticias_banner_l extends WP_Widget {

	function __construct() {
		parent::__construct('dos_noticias_banner_l', '2 Noticias y Banner Izquierda', array('description' => __('Formatos posibles: .jpg, .gif, .html'),
			'post_L' => '',
			'post_R' => '',
			'color_L' => '',
			'color_R' => '',
            'banner' => ''
			)
		);
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['post_L'] = strip_tags($new_instance['post_L']);
		$instance['post_R'] = strip_tags($new_instance['post_R']);
        $instance['color_L'] = strip_tags($new_instance['color_L']);
        $instance['color_R'] = strip_tags($new_instance['color_R']);
		$instance['banner'] = strip_tags($new_instance['banner']);
		return $instance;
	}


	// Backend part of the widget
	public function form($instance) {
		if($instance) {
			$post_L = esc_attr($instance['post_L']);
			$post_R = esc_attr($instance['post_R']);
            $color_L = esc_attr($instance['color_L']);
            $color_R = esc_attr($instance['color_R']);
			$banner = esc_attr($instance['banner']);
		} else {

		}

		?>

            <p>
				<label for="<?php echo $this->get_field_id('post_L');?>">Noticia Izquierda</label>
                <?= auto_complete('noticia', $this->get_field_id('post_L'), $this->get_field_name('post_L'), $post_L.'_autocomplete', $post_L, get_post_field('post_title',$post_L)); ?>

                <div style="margin-top: 10px;">
                    <input id="<?php echo ($this->get_field_id( 'color_L' ) . '-3') ?>" name="<?php echo $this->get_field_name('color_L'); ?>" value="3" type="radio" <?php checked( $color_L == 3, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_L' ) . '-3') ?>" style="position: relative; top: -2px; left: -3px;">Azul</label>
                    <input id="<?php echo ($this->get_field_id( 'color_L' ) . '-2') ?>" name="<?php echo $this->get_field_name('color_L'); ?>" value="2" type="radio" <?php checked( $color_L == 2, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_L' ) . '-2') ?>" style="position: relative; top: -2px; left: -3px;">Naranja</label>
                    <input id="<?php echo ($this->get_field_id( 'color_L' ) . '-1') ?>" name="<?php echo $this->get_field_name('color_L'); ?>" value="1" type="radio" <?php checked( $color_L == 1, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_L' ) . '-1') ?>" style="position: relative; top: -2px; left: -3px;">Verde</label>
                </div>

            </p>
            <p>
				<label for="<?php echo $this->get_field_id('post_R');?>">Noticia Derecha</label>
                <?= auto_complete('noticia', $this->get_field_id('post_R'), $this->get_field_name('post_R'), $post_R.'_autocomplete', $post_R, get_post_field('post_title',$post_R)); ?>
                <div style="margin-top: 10px;">
                    <input id="<?php echo ($this->get_field_id( 'color_R' ) . '-3') ?>" name="<?php echo $this->get_field_name('color_R'); ?>" value="3" type="radio" <?php checked( $color_R == 3, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_R' ) . '-3') ?>" style="position: relative; top: -2px; left: -3px;">Azul</label>
                    <input id="<?php echo ($this->get_field_id( 'color_R' ) . '-2') ?>" name="<?php echo $this->get_field_name('color_R'); ?>" value="2" type="radio" <?php checked( $color_R == 2, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_R' ) . '-2') ?>" style="position: relative; top: -2px; left: -3px;">Naranja</label>
                    <input id="<?php echo ($this->get_field_id( 'color_R' ) . '-1') ?>" name="<?php echo $this->get_field_name('color_R'); ?>" value="1" type="radio" <?php checked( $color_R == 1, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_R' ) . '-1') ?>" style="position: relative; top: -2px; left: -3px;">Verde</label>
                </div>
			</p>

            <h4>Banner a la izquierda</h4>
            <p>
                <h4>Tamaño único: 300px de ancho x 250px de alto</h4>
				<label for="<?php echo $this->get_field_id('banner');?>">Seleccione Banner</label>
                <?= auto_complete('banner', $this->get_field_id('banner'), $this->get_field_name('banner'), $banner.'_autocomplete', $banner, get_post_field('post_title',$banner)); ?>
			</p>

		<?php
	}

	// Frontend part of the widget
	function widget($args, $instance) {
		$post_L = apply_filters('post_L', $instance['post_L']);
		$post_R = apply_filters('post_R', $instance['post_R']);
        $color_L = apply_filters('color_L', $instance['color_L']);
        $color_R = apply_filters('color_R', $instance['color_R']);

		$banner = apply_filters('banner', $instance['banner']);
        $banner_link = get_post_meta($banner, 'link_meta_url', true);
		$banner_img = get_post_meta($banner, 'banner_meta_tag', true);

		?>

		<div class="row dos-noticias">
			<div class="container">

                <div class="col-md-4 gutter-sm banner-noticia banner-center">
                    <a href="<?php echo $banner_link ?>>" target="_blank"><?php echo $banner_img ?></a>
                </div>

                <div class="col-md-4 gutter-sm">
					<?php echo getNoticias($post_L, 'col4', $color_L) ?>
                </div>

                <div class="col-md-4 gutter-sm">
					<?php echo getNoticias($post_R, 'col4', $color_R) ?>
                </div>

			</div>
		</div>

		<?php
	}
}

?>
