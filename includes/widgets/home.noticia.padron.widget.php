<?php

class noticia_padron extends WP_Widget {

	function __construct() {
		parent::__construct('noticia_padron', 'Noticia + Padron', array('description' => __('Noticia + Padron'),
			'post_L' => '',
			'color_L' => '',
			)
		);
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['post_L'] = strip_tags($new_instance['post_L']);
		$instance['color_L'] = strip_tags($new_instance['color_L']);
		return $instance;
	}


	// Backend part of the widget
	public function form($instance) {
		if($instance) {
			$post_L = esc_attr($instance['post_L']);
			$color_L = esc_attr($instance['color_L']);
		} else {

		}

		?>

            <p>
				<label for="<?php echo $this->get_field_id('post_L');?>">Noticia Izquierda</label>
                <?= auto_complete('noticia', $this->get_field_id('post_L'), $this->get_field_name('post_L'), $post_L.'_autocomplete', $post_L, get_post_field('post_title',$post_L)); ?>

                <div style="margin-top: 10px;">
                    <input id="<?php echo ($this->get_field_id( 'color_L' ) . '-3') ?>" name="<?php echo $this->get_field_name('color_L'); ?>" value="3" type="radio" <?php checked( $color_L == 3, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_L' ) . '-3') ?>" style="position: relative; top: -2px; left: -3px;">Azul</label>
                    <input id="<?php echo ($this->get_field_id( 'color_L' ) . '-2') ?>" name="<?php echo $this->get_field_name('color_L'); ?>" value="2" type="radio" <?php checked( $color_L == 2, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_L' ) . '-2') ?>" style="position: relative; top: -2px; left: -3px;">Naranja</label>
                    <input id="<?php echo ($this->get_field_id( 'color_L' ) . '-1') ?>" name="<?php echo $this->get_field_name('color_L'); ?>" value="1" type="radio" <?php checked( $color_L == 1, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_L' ) . '-1') ?>" style="position: relative; top: -2px; left: -3px;">Verde</label>
                </div>
			</p>


		<?php
	}

	// Frontend part of the widget
	function widget($args, $instance) {
		$post_L = apply_filters('post_L', $instance['post_L']);
		$color_L = apply_filters('color_L', $instance['color_L']);

		?>

		<div class="row dos-noticias">
			<div class="container">

                <div class="col-md-6 gutter-sm">
	                <?php echo getNoticias($post_L, 'col6', $color_L) ?>
                </div>

                <div class="col-md-6 gutter-sm">
                    <a href="/padron-electoral"><img src="<?php bloginfo('stylesheet_directory');?>/images/padron.jpg" width="100%" height="100%"></a>
                </div>

			</div>
		</div>

		<?php
	}
}

?>
