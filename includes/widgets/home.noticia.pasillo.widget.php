<?php

class noticia_pasillo extends WP_Widget {

	function __construct() {
		parent::__construct('noticia_pasillo', 'Noticia + Pasillo', array('description' => __('Noticia común + Nota de Pasillo Roverano'),
			'post_L' => '',
			'post_R' => '',
			'color_R' => '',
			)
		);
	}

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['post_L'] = intval( $new_instance['post_L'] );
        $instance['post_R'] = intval( $new_instance['post_R'] );
        $instance['color_R'] = intval( $new_instance['color_R'] );
        return $instance;
    }


	// Backend part of the widget
	public function form($instance) {
		if($instance) {
            $post_L = (isset($instance['post_L']))?intval($instance['post_L']):'';
            $post_R = (isset($instance['post_R']))?intval($instance['post_R']):'';
            $color_R = (isset($instance['color_R']))?intval($instance['color_R']):'';
		} else {

		}
		?>

            <p>
				<label for="<?php echo $this->get_field_id('post_L');?>">Nota de Pasillo Roverano</label>
		        <?php auto_complete('pasillo', $this->get_field_id('post_L'), $this->get_field_name('post_L'), $post_L.'_autocomplete', $post_L, get_post_field('post_title',$post_L)); ?>
			</p>

            <p>
				<label for="<?php echo $this->get_field_id('post_R');?>">Noticia Derecha</label>
                <?= auto_complete('noticia', $this->get_field_id('post_R'), $this->get_field_name('post_R'), $post_R.'_autocomplete', $post_R, get_post_field('post_title',$post_R)); ?>
                <div style="margin-top: 10px;">
                    <input id="<?php echo ($this->get_field_id( 'color_R' ) . '-3') ?>" name="<?php echo $this->get_field_name('color_R'); ?>" value="3" type="radio" <?php checked( $color_R == 3, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_R' ) . '-3') ?>" style="position: relative; top: -2px; left: -3px;">Azul</label>
                    <input id="<?php echo ($this->get_field_id( 'color_R' ) . '-2') ?>" name="<?php echo $this->get_field_name('color_R'); ?>" value="2" type="radio" <?php checked( $color_R == 2, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_R' ) . '-2') ?>" style="position: relative; top: -2px; left: -3px;">Naranja</label>
                    <input id="<?php echo ($this->get_field_id( 'color_R' ) . '-1') ?>" name="<?php echo $this->get_field_name('color_R'); ?>" value="1" type="radio" <?php checked( $color_R == 1, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_R' ) . '-1') ?>" style="position: relative; top: -2px; left: -3px;">Verde</label>
                </div>
			</p>

		<?php
	}

	// Frontend part of the widget
	function widget($args, $instance) {
		$post_L = apply_filters('post_L', $instance['post_L']);
		$post_R = apply_filters('post_R', $instance['post_R']);
		$color_R = apply_filters('color_R', $instance['color_R']);

		?>
<!--        add comment to check-->
		<div class="row dos-noticias">
			<div class="container">

                <div class="col-md-6 gutter-sm">
					<?php echo getNoticias($post_L, 'col6', '107,189,81') ?>
                </div>

                <div class="col-md-6 gutter-sm">
					<?php echo getNoticias($post_R, 'col6', $color_R) ?>
                </div>

			</div>
		</div>

		<?php
	}
}

?>
