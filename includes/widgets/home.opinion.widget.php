<?php

class home_opinion extends WP_Widget {

	function __construct() {
		parent::__construct('home_opinion', 'Columnas de Opinión', array('description' => __('Slider con columnas de opinión'),
			'opinion01' => '',
			'opinion02' => '',
			'opinion03' => '',
			'opinion04' => ''
            )
		);
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['opinion01'] = strip_tags($new_instance['opinion01']);
		$instance['opinion02'] = strip_tags($new_instance['opinion02']);
		$instance['opinion03'] = strip_tags($new_instance['opinion03']);
		$instance['opinion04'] = strip_tags($new_instance['opinion04']);
		return $instance;
	}


	// Backend part of the widget
	public function form($instance) {
		if($instance) {
			$opinion01 = esc_attr($instance['opinion01']);
			$opinion02 = esc_attr($instance['opinion02']);
			$opinion03 = esc_attr($instance['opinion03']);
			$opinion04 = esc_attr($instance['opinion04']);
		} else {

		}

		?>
			<p>
				<label for="<?php echo $this->get_field_id('opinion01');?>">Opinión Nro 1</label>
                <?= auto_complete('opinion', $this->get_field_id('opinion01'), $this->get_field_name('opinion01'), $opinion01.'_autocomplete', $opinion01, get_post_field('post_title',$opinion01)); ?>

            </p>

			<p>
				<label for="<?php echo $this->get_field_id('opinion02');?>">Opinión Nro 2</label>
                <?= auto_complete('opinion', $this->get_field_id('opinion02'), $this->get_field_name('opinion02'), $opinion02.'_autocomplete', $opinion02, get_post_field('post_title',$opinion02)); ?>

            </p>

			<p>
				<label for="<?php echo $this->get_field_id('opinion03');?>">Opinión Nro 3</label>
                <?= auto_complete('opinion', $this->get_field_id('opinion03'), $this->get_field_name('opinion03'), $opinion03.'_autocomplete', $opinion03, get_post_field('post_title',$opinion03)); ?>

            </p>

			<p>
				<label for="<?php echo $this->get_field_id('opinion04');?>">Opinión Nro 4</label>
                <?= auto_complete('opinion', $this->get_field_id('opinion04'), $this->get_field_name('opinion04'), $opinion04.'_autocomplete', $opinion04, get_post_field('post_title',$opinion04)); ?>

            </p>

		<?php
	}

	// Frontend part of the widget
	function widget($args, $instance) {
		$opinion01 = apply_filters('opinion01', $instance['opinion01']);
		$opinion02 = apply_filters('opinion02', $instance['opinion02']);
		$opinion03 = apply_filters('opinion03', $instance['opinion03']);
		$opinion04 = apply_filters('opinion04', $instance['opinion04']);

		$posts = array($opinion01, $opinion02, $opinion03, $opinion04);

		$args = array(
			'post__in' => $posts,
			'posts_per_page' => 4,
			'post_type' => 'opinion',
			'post_status' => 'publish'
		);

		$query = new WP_Query($args);

		?>

		<div class="row opinion">
			<div class="container">
				<div class="col-md-12">
					<h4>Columnas de opinión</h4>
				</div>
				<?php

				if($query->have_posts()) :
					echo '<div class="row">';
					while($query->have_posts()) : $query->the_post();
						$authorAvatar = get_avatar_url(get_the_author_meta('ID'));
						$authorFullName = get_the_author_meta('display_name', get_the_author_meta('ID'));
						$postTitle = get_the_title();

						?>

						<a href="<?php echo get_permalink() ?>">
                            <div class="col-md-3 opinion-item">
                                <div class="opi-author-avatar">
                                    <div class="opi-author-photo" style="background-image: url('<?php echo $authorAvatar ?>')"></div>
                                </div>
                                <div>
                                    <h4><?php echo $postTitle ?></h4>
                                    <h5>Por <?php echo $authorFullName ?></h5>
                                </div>
                            </div>
						</a>

                         <?php

					endwhile;
					echo '</div>';
					wp_reset_postdata();
				endif;

				?>
			</div>
		</div>

		<?php
	}
}

?>
