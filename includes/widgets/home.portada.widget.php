<?php

class home_portada extends WP_Widget {

	function __construct() {
		parent::__construct('home_portada', 'Portada', array('description' => __('Portada de 3 Noticias para la home'),
			'post_L' => '',
			'post_M' => '',
			'post_R' => '',
			'color_L' => '',
			'color_M' => '',
			'color_R' => ''
			)
		);
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['post_L'] = strip_tags($new_instance['post_L']);
		$instance['post_M'] = strip_tags($new_instance['post_M']);
		$instance['post_R'] = strip_tags($new_instance['post_R']);
		$instance['color_L'] = strip_tags($new_instance['color_L']);
		$instance['color_M'] = strip_tags($new_instance['color_M']);
		$instance['color_R'] = strip_tags($new_instance['color_R']);
		return $instance;
	}


	// Backend part of the widget
	public function form($instance) {
		if($instance) {
			$post_L = esc_attr($instance['post_L']);
			$post_M = esc_attr($instance['post_M']);
			$post_R = esc_attr($instance['post_R']);
			$color_L = esc_attr($instance['color_L']);
			$color_M = esc_attr($instance['color_M']);
			$color_R = esc_attr($instance['color_R']);
		} else {

		}

		?>
			<p>
				<label for="<?php echo $this->get_field_id('post_L');?>">Noticia 1</label>
                <?= auto_complete('noticia', $this->get_field_id('post_L'), $this->get_field_name('post_L'), $post_L.'_autocomplete', $post_L, get_post_field('post_title',$post_L)); ?>
                <div style="margin-top: 10px;">
                    <input id="<?php echo ($this->get_field_id( 'color_L' ) . '-3') ?>" name="<?php echo $this->get_field_name('color_L'); ?>" value="3" type="radio" <?php checked( $color_L == 3, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_L' ) . '-3') ?>" style="position: relative; top: -2px; left: -3px;">Azul</label>
                    <input id="<?php echo ($this->get_field_id( 'color_L' ) . '-2') ?>" name="<?php echo $this->get_field_name('color_L'); ?>" value="2" type="radio" <?php checked( $color_L == 2, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_L' ) . '-2') ?>" style="position: relative; top: -2px; left: -3px;">Naranja</label>
                    <input id="<?php echo ($this->get_field_id( 'color_L' ) . '-1') ?>" name="<?php echo $this->get_field_name('color_L'); ?>" value="1" type="radio" <?php checked( $color_L == 1, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_L' ) . '-1') ?>" style="position: relative; top: -2px; left: -3px;">Verde</label>
                </div>

			</p>

			<p>
				<label for="<?php echo $this->get_field_id('post_M');?>">Noticia 2</label>
                <?= auto_complete('noticia', $this->get_field_id('post_M'), $this->get_field_name('post_M'), $post_M.'_autocomplete', $post_M, get_post_field('post_title',$post_M)); ?>
                <div style="margin-top: 10px;">
                    <input id="<?php echo ($this->get_field_id( 'color_M' ) . '-3') ?>" name="<?php echo $this->get_field_name('color_M'); ?>" value="3" type="radio" <?php checked( $color_M == 3, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_M' ) . '-3') ?>" style="position: relative; top: -2px; left: -3px;">Azul</label>
                    <input id="<?php echo ($this->get_field_id( 'color_M' ) . '-2') ?>" name="<?php echo $this->get_field_name('color_M'); ?>" value="2" type="radio" <?php checked( $color_M == 2, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_M' ) . '-2') ?>" style="position: relative; top: -2px; left: -3px;">Naranja</label>
                    <input id="<?php echo ($this->get_field_id( 'color_M' ) . '-1') ?>" name="<?php echo $this->get_field_name('color_M'); ?>" value="1" type="radio" <?php checked( $color_M == 1, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_M' ) . '-1') ?>" style="position: relative; top: -2px; left: -3px;">Verde</label>
                </div>

			</p>

			<p>
				<label for="<?php echo $this->get_field_id('post_R');?>">Noticia 3</label>
                <?= auto_complete('noticia', $this->get_field_id('post_R'), $this->get_field_name('post_R'), $post_R.'_autocomplete', $post_R, get_post_field('post_title',$post_R)); ?>
                <div style="margin-top: 10px;">
                    <input id="<?php echo ($this->get_field_id( 'color_R' ) . '-3') ?>" name="<?php echo $this->get_field_name('color_R'); ?>" value="3" type="radio" <?php checked( $color_R == 3, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_R' ) . '-3') ?>" style="position: relative; top: -2px; left: -3px;">Azul</label>
                    <input id="<?php echo ($this->get_field_id( 'color_R' ) . '-2') ?>" name="<?php echo $this->get_field_name('color_R'); ?>" value="2" type="radio" <?php checked( $color_R == 2, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_R' ) . '-2') ?>" style="position: relative; top: -2px; left: -3px;">Naranja</label>
                    <input id="<?php echo ($this->get_field_id( 'color_R' ) . '-1') ?>" name="<?php echo $this->get_field_name('color_R'); ?>" value="1" type="radio" <?php checked( $color_R == 1, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_R' ) . '-1') ?>" style="position: relative; top: -2px; left: -3px;">Verde</label>
                </div>

			</p>

		<?php
	}

	// Frontend part of the widget
	function widget($args, $instance) {

		$post_L = apply_filters('post_L', $instance['post_L']);
		$post_M = apply_filters('post_M', $instance['post_M']);
		$post_R = apply_filters('post_R', $instance['post_R']);
        $color_L = apply_filters('color_L', $instance['color_L']);
		$color_M = apply_filters('color_M', $instance['color_M']);
		$color_R = apply_filters('color_R', $instance['color_R']);

		?>

        <div class="row" style="margin-top: 10px;">
            <div class="container">

                <div class="col-md-8 gutter-sm">
                    <?php echo getNoticias($post_L, 'portada-principal', $color_L) ?>
                </div>

                <div class="col-md-4 gutter-sm">
	                <?php echo getNoticias($post_M, 'portada-secundaria', $color_M) ?>
	            </div>
                 <div class="col-md-4 gutter-sm">
	                <?php echo getNoticias($post_R, 'portada-secundaria', $color_R) ?>
                </div>

            </div>
        </div>

        <?php
	}
}



?>
