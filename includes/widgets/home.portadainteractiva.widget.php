<?php

class home_portada_unica extends WP_Widget {

	function __construct() {
		parent::__construct('home_portada_unica', 'Portada Interactiva', array('description' => __('Portada con slider de noticias y columnas con animacion'),
			'post_L' => '',
			'post_M' => '',
			'post_R' => '',
			'color_L' => '',
			'color_M' => '',
			'color_R' => '',
            'columna_1' => '',
            'columna_2' => '',
            'columna_3' => '',
            'columna_4' => '',
			)
		);
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['post_L'] = strip_tags($new_instance['post_L']);
		$instance['post_M'] = strip_tags($new_instance['post_M']);
		$instance['post_R'] = strip_tags($new_instance['post_R']);
		$instance['color_L'] = strip_tags($new_instance['color_L']);
		$instance['color_M'] = strip_tags($new_instance['color_M']);
		$instance['color_R'] = strip_tags($new_instance['color_R']);
		$instance['columna_1'] = strip_tags($new_instance['columna_1']);
		$instance['columna_2'] = strip_tags($new_instance['columna_2']);
		$instance['columna_3'] = strip_tags($new_instance['columna_3']);
		$instance['columna_4'] = strip_tags($new_instance['columna_4']);
		return $instance;
	}


	// Backend part of the widget
	public function form($instance) {
		if($instance) {
			$post_L = esc_attr($instance['post_L']);
			$post_M = esc_attr($instance['post_M']);
			$post_R = esc_attr($instance['post_R']);
			$color_L = esc_attr($instance['color_L']);
			$color_M = esc_attr($instance['color_M']);
			$color_R = esc_attr($instance['color_R']);
			$columna_1 = esc_attr($instance['columna_1']);
			$columna_2 = esc_attr($instance['columna_2']);
			$columna_3 = esc_attr($instance['columna_3']);
			$columna_4 = esc_attr($instance['columna_4']);
		} else {

		}

		?>
            <h3>Noticias para Slider</h3>
			<p>
				<label for="<?php echo $this->get_field_id('post_L');?>">Noticia 1</label>
                <?= auto_complete('noticia', $this->get_field_id('post_L'), $this->get_field_name('post_L'), $post_L.'_autocomplete', $post_L, get_post_field('post_title',$post_L)); ?>
                <div style="margin-top: 10px;">
                    <input id="<?php echo ($this->get_field_id( 'color_L' ) . '-3') ?>" name="<?php echo $this->get_field_name('color_L'); ?>" value="3" type="radio" <?php checked( $color_L == 3, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_L' ) . '-3') ?>" style="position: relative; top: -2px; left: -3px;">Azul</label>
                    <input id="<?php echo ($this->get_field_id( 'color_L' ) . '-2') ?>" name="<?php echo $this->get_field_name('color_L'); ?>" value="2" type="radio" <?php checked( $color_L == 2, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_L' ) . '-2') ?>" style="position: relative; top: -2px; left: -3px;">Naranja</label>
                    <input id="<?php echo ($this->get_field_id( 'color_L' ) . '-1') ?>" name="<?php echo $this->get_field_name('color_L'); ?>" value="1" type="radio" <?php checked( $color_L == 1, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_L' ) . '-1') ?>" style="position: relative; top: -2px; left: -3px;">Verde</label>
                </div>

			</p>

			<p>
				<label for="<?php echo $this->get_field_id('post_M');?>">Noticia 2</label>
                <?= auto_complete('noticia', $this->get_field_id('post_M'), $this->get_field_name('post_M'), $post_M.'_autocomplete', $post_M, get_post_field('post_title',$post_M)); ?>
                <div style="margin-top: 10px;">
                    <input id="<?php echo ($this->get_field_id( 'color_M' ) . '-3') ?>" name="<?php echo $this->get_field_name('color_M'); ?>" value="3" type="radio" <?php checked( $color_M == 3, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_M' ) . '-3') ?>" style="position: relative; top: -2px; left: -3px;">Azul</label>
                    <input id="<?php echo ($this->get_field_id( 'color_M' ) . '-2') ?>" name="<?php echo $this->get_field_name('color_M'); ?>" value="2" type="radio" <?php checked( $color_M == 2, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_M' ) . '-2') ?>" style="position: relative; top: -2px; left: -3px;">Naranja</label>
                    <input id="<?php echo ($this->get_field_id( 'color_M' ) . '-1') ?>" name="<?php echo $this->get_field_name('color_M'); ?>" value="1" type="radio" <?php checked( $color_M == 1, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_M' ) . '-1') ?>" style="position: relative; top: -2px; left: -3px;">Verde</label>
                </div>

			</p>

			<p>
				<label for="<?php echo $this->get_field_id('post_R');?>">Noticia 3</label>
                <?= auto_complete('noticia', $this->get_field_id('post_R'), $this->get_field_name('post_R'), $post_R.'_autocomplete', $post_R, get_post_field('post_title',$post_R)); ?>
                <div style="margin-top: 10px;">
                    <input id="<?php echo ($this->get_field_id( 'color_R' ) . '-3') ?>" name="<?php echo $this->get_field_name('color_R'); ?>" value="3" type="radio" <?php checked( $color_R == 3, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_R' ) . '-3') ?>" style="position: relative; top: -2px; left: -3px;">Azul</label>
                    <input id="<?php echo ($this->get_field_id( 'color_R' ) . '-2') ?>" name="<?php echo $this->get_field_name('color_R'); ?>" value="2" type="radio" <?php checked( $color_R == 2, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_R' ) . '-2') ?>" style="position: relative; top: -2px; left: -3px;">Naranja</label>
                    <input id="<?php echo ($this->get_field_id( 'color_R' ) . '-1') ?>" name="<?php echo $this->get_field_name('color_R'); ?>" value="1" type="radio" <?php checked( $color_R == 1, true) ?> />
                    <label for="<?php echo ($this->get_field_id( 'color_R' ) . '-1') ?>" style="position: relative; top: -2px; left: -3px;">Verde</label>
                </div>

			</p>

            <h3>Noticias para Columna Animada</h3>

            <p>
                <label for="<?php echo $this->get_field_id('columna_1');?>">Noticia 1</label>
                <?= auto_complete('noticia', $this->get_field_id('columna_1'), $this->get_field_name('columna_1'), $columna_1.'_autocomplete', $columna_1, get_post_field('post_title',$columna_1)); ?>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('columna_2');?>">Noticia 1</label>
                <?= auto_complete('noticia', $this->get_field_id('columna_2'), $this->get_field_name('columna_2'), $columna_2.'_autocomplete', $columna_2, get_post_field('post_title',$columna_2)); ?>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('columna_3');?>">Noticia 1</label>
                <?= auto_complete('noticia', $this->get_field_id('columna_3'), $this->get_field_name('columna_3'), $columna_3.'_autocomplete', $columna_3, get_post_field('post_title',$columna_3)); ?>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('columna_4');?>">Noticia 1</label>
                <?= auto_complete('noticia', $this->get_field_id('columna_4'), $this->get_field_name('columna_4'), $columna_4.'_autocomplete', $columna_4, get_post_field('post_title',$columna_4)); ?>
            </p>

		<?php
	}

	// Frontend part of the widget
	function widget($args, $instance) {

		$post_L = apply_filters('post_L', $instance['post_L']);
		$post_M = apply_filters('post_M', $instance['post_M']);
		$post_R = apply_filters('post_R', $instance['post_R']);
        $color_L = apply_filters('color_L', $instance['color_L']);
		$color_M = apply_filters('color_M', $instance['color_M']);
		$color_R = apply_filters('color_R', $instance['color_R']);
		$columna_1 = apply_filters('columna_1', $instance['columna_1']);
		$columna_2 = apply_filters('columna_2', $instance['columna_2']);
		$columna_3 = apply_filters('columna_3', $instance['columna_3']);
		$columna_4 = apply_filters('columna_4', $instance['columna_4']);

		?>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#lightSlider-Portada").lightSlider({
                    item: 1,
                    autoWidth: false,
                    slideMove: 1, // slidemove will be 1 if loop is true
                    slideMargin: 10,

                    addClass: '',
                    mode: "slide",
                    useCSS: true,
                    cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                    easing: 'linear', //'for jquery animation',////

                    speed: 400, //ms'
                    auto: true,
                    loop: true,
                    slideEndAnimation: true,
                    pause: 9000,

                    keyPress: true,
                    controls: true,
                    prevHtml: '<img style= "margin-left:20px;" height="35px" src="<?php bloginfo('stylesheet_directory');?>/images/arrow-prev.svg" />',
                    nextHtml: '<img style= "margin:-20px;" height="35px" src="<?php bloginfo('stylesheet_directory');?>/images/arrow-next.svg" />',

                    rtl:false,
                    adaptiveHeight:false,

                    vertical:false,
                    verticalHeight:500,
                    vThumbWidth:100,

                    thumbItem:10,
                    pager: false,
                    gallery: false,
                    galleryMargin: 5,
                    thumbMargin: 5,
                    currentPagerPosition: 'middle',

                    enableTouch:true,
                    enableDrag:false,
                    freeMove:true,
                    swipeThreshold: 40,

                    responsive : [],

                    onBeforeStart: function (el) {},
                    onSliderLoad: function (el) {},
                    onBeforeSlide: function (el) {},
                    onAfterSlide: function (el) {},
                    onBeforeNextSlide: function (el) {},
                    onBeforePrevSlide: function (el) {}
                });
            });
        </script>

        <div class="row" style="margin-top: 10px;">
            <div class="container">

                <div class="col-md-8 gutter-sm">
                    <ul id="lightSlider-Portada">
                        <li><?php echo getNoticias($post_M, 'portada-principal', $color_M) ?></li>
                        <li><?php echo getNoticias($post_L, 'portada-principal', $color_L) ?></li>
                        <li><?php echo getNoticias($post_R, 'portada-principal', $color_R) ?></li>
                    </ul>
                </div>

                <div class="col-md-4 gutter-sm">
                    <link href="https://fonts.googleapis.com/css?family=Teko:300" rel="stylesheet">
                    <div class="columna-interactiva-header">
                        NOTICIAS DESTACADAS
                    </div>

	                <?php

	                $args = array(
		                'post_type' => 'noticia',
		                'post__in' => [$columna_1, $columna_2, $columna_3, $columna_4],
		                'orderby' => 'date',
		                'order' => 'DESC',
		                'post_status' => 'publish'
	                );

	                $i = 1;

	                $query = new WP_Query($args);
	                if($query->have_posts()) :
		                while($query->have_posts()) : $query->the_post();
			                ?>
                            <a href="<?php echo get_the_permalink() ?>">
                                <div onmouseenter="" class="columna-interactiva-noticia">
                                    <div class="columna-interactiva-placa">
                                        <span class="columna-interactiva-numero"><?php echo $i ?></span>
                                      <span class="columna-interactiva-titular"><?php echo get_the_title(); ?></span>
                                    </div>
                                    <div class="fondo"><img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'portada-interactiva') ?>" width="100%"></div>

                                </div>
                            </a>
                            <?php

                        $i++;

		                endwhile;
		                wp_reset_postdata();
	                endif;

	                ?>

                </div>
            </div>
        </div>

        <?php
	}
}
?>