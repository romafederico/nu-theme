<?php

class noticias_secundarias extends WP_Widget {

	function __construct() {
		parent::__construct('noticias_secundarias', 'Noticias Secundarias', array('description' => __('3 Noticias de menor importancia'),
			'post_L' => '',
			'post_M' => '',
			'post_R' => ''
			)
		);
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['post_L'] = strip_tags($new_instance['post_L']);
		$instance['post_M'] = strip_tags($new_instance['post_M']);
		$instance['post_R'] = strip_tags($new_instance['post_R']);
		return $instance;
	}


	// Backend part of the widget
	public function form($instance) {
		if($instance) {
			$post_L = esc_attr($instance['post_L']);
			$post_M = esc_attr($instance['post_M']);
			$post_R = esc_attr($instance['post_R']);
		} else {

		}

		?>
			<p>
				<label for="<?php echo $this->get_field_id('post_L');?>">Noticia Izquierda</label>
                <?= auto_complete('noticia', $this->get_field_id('post_L'), $this->get_field_name('post_L'), $post_L.'_autocomplete', $post_L, get_post_field('post_title',$post_L)); ?>
            </p>

			<p>
				<label for="<?php echo $this->get_field_id('post_M');?>">Noticia Central</label>
                <?= auto_complete('noticia', $this->get_field_id('post_M'), $this->get_field_name('post_M'), $post_M.'_autocomplete', $post_M, get_post_field('post_title',$post_M)); ?>
            </p>

			<p>
				<label for="<?php echo $this->get_field_id('post_R');?>">Noticia Derecha</label>
                <?= auto_complete('noticia', $this->get_field_id('post_R'), $this->get_field_name('post_R'), $post_R.'_autocomplete', $post_R, get_post_field('post_title',$post_R)); ?>
			</p>

		<?php
	}

	// Frontend part of the widget
	function widget($args, $instance) {
		$post_L = apply_filters('post_L', $instance['post_L']);
		$post_M = apply_filters('post_M', $instance['post_M']);
		$post_R = apply_filters('post_R', $instance['post_R']);
		$posts = array($post_L, $post_M, $post_R);

		$args = array(
			'post__in' => $posts,
			'posts_per_page' => 3,
			'post_type' => 'noticia',
			'post_status' => 'publish'
		);

		$query = new WP_Query($args);

		?>

		<div class="row secundarias">
			<div class="container">
				<div class="col-md-12">
					<h3><img src="<?php bloginfo('stylesheet_directory');?>/images/icon-secundarias.svg" height="50"> Qué más está pasando...</h3>
				</div>

			<?php

			if($query->have_posts()) :
				$i = 1;
				while($query->have_posts()) : $query->the_post();
					echo '<a href="' . get_post_permalink() . '">';
					echo '<div class="col-sm-4">';
					echo '<img src="' . get_the_post_thumbnail_url(get_the_ID(), 'secundarias') . '" width="100%">';
					echo '<div class="col-xs-1"><h2>' . $i . '</h2></div><div class="col-xs-10"><h4>' . get_the_title() . '</h4></div>';
					echo '</div>';
					echo '</a>';
					$i++;
				endwhile;
				wp_reset_postdata();
			endif;

			?>

			</div>
		</div>
		<?php

	}
}

?>
