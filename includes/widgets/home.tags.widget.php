<?php


class home_tags extends WP_Widget {

    function __construct() {
		parent::__construct('home_tags', 'Tags', array('description' => __('Listado de Tags mas populares para la Home')));
	}

    public function form($instance) {
        ?>
        <p>Se mostraran los Tags seleccionados en las <a href="themes.php?page=theme_options">Opciones del Tema</a></p>
        <?php
    }

    public function update($new_instance, $old_instance) {
        return $new_instance;
    }

    public function widget($args, $instance) {
        extract($args);

        $tag_ids = theme_options('tag_ids');
        if ($tag_ids) {
            foreach ($tag_ids as $tag_id) {
                $tags[] = get_tag($tag_id);
            }
        } else {
            $tags = get_tags(array('orderby'=>'count','order'=>'DESC','number'=>5));
        }
        ?>
        <div class="container">
            <div class="tags">
                <?php
                foreach ($tags as $tag) {
                     ?>
                     <span class="tags-item"><a href="<?= get_tag_link($tag->term_id); ?>" title="<?= $tag->name; ?> Tag" class="<?= $tag->slug; ?>"><?= $tag->name; ?></a></span>
                    <!-- <li class="tags-item"><a href="<?= get_tag_link($tag->term_id); ?>" title="<?= $tag->name; ?> Tag" class="<?= $tag->slug; ?>"><?= $tag->name; ?></a></li> -->
                    <?php
                }
                ?>

            </div>
        </div>
        <?php
    }
}

?>
