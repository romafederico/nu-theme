<?php

class home_tapas extends WP_Widget {

    function __construct() {
		parent::__construct('home_tapas', 'Tapas', array('description' => __('Tapas de Revistas/Diarios para la Home')));
	}

    public function form($instance)
    {
        $cant_tapas = (isset($instance['cant_tapas']))?intval($instance['cant_tapas']):10;
        ?>
            <p>Indique la Cantidad de Tapas de Revistas/Diarios a mostrar:</p>
            <p><input type="text" value="<?= $cant_tapas; ?>"  id="<?= $this->get_field_id('cant_tapas'); ?>" name="<?= $this->get_field_name('cant_tapas'); ?>" /></p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['cant_tapas'] = intval($new_instance['cant_tapas']);
        return $instance;
    }

    public function widget($args, $instance)
    {
        extract($args);
        $cant_tapas  = $instance['cant_tapas'];

        $tapas = get_posts(array(
            'post_type' => 'tapa',
            'posts_per_page' => 20,
            'numberposts' => $cant_tapas
        ));

        if ($tapas):
        ?>
        <script type="text/javascript">
                $(document).ready(function() {
                    $("#lightSlider-tapas").lightSlider({
                        item: 6,
                        autoWidth: false,
                        slideMove: 1, // slidemove will be 1 if loop is true
                        slideMargin: 10,

                        addClass: '',
                        mode: "slide",
                        useCSS: true,
                        cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                        easing: 'linear', //'for jquery animation',////

                        speed: 400, //ms'
                        auto: false,
                        loop: true,
                        slideEndAnimation: true,
                        pause: 9000,

                        keyPress: true,
                        controls: true,
                        prevHtml: '<img style= "margin-left:20px;" height="35px" src="<?php bloginfo('stylesheet_directory');?>/images/arrow-prev.svg" />',
                        nextHtml: '<img style= "margin:-20px;" height="35px" src="<?php bloginfo('stylesheet_directory');?>/images/arrow-next.svg" />',

                        rtl:false,
                        adaptiveHeight:false,

                        vertical:false,
                        verticalHeight:500,
                        vThumbWidth:100,

                        thumbItem:20,
                        pager: false,
                        gallery: false,
                        galleryMargin: 5,
                        thumbMargin: 5,
                        currentPagerPosition: 'middle',

                        enableTouch:true,
                        enableDrag:false,
                        freeMove:true,
                        swipeThreshold: 40,

                        responsive : [],

                        onBeforeStart: function (el) {},
                        onSliderLoad: function (el) {},
                        onBeforeSlide: function (el) {},
                        onAfterSlide: function (el) {},
                        onBeforeNextSlide: function (el) {},
                        onBeforePrevSlide: function (el) {}
                    });
                });
            </script>
            <div class="row tapa-slider hidden-xs hidden-sm">
                <div class="container ">
                    <ul id="lightSlider-tapas">
                        <?php
                        foreach ($tapas as $tapa) {
                            echo '<a href="' . get_post_meta($tapa->ID, 'link_meta_url', true) . '" target="_blank">';
                            echo '<li><img src="' .  get_the_post_thumbnail_url($tapa->ID) . '"></li>';
                            echo '</a>';
                        }
                        ?>
                    </ul>
                </div>
            </div>

        </div>
        <div style="clear: both;"></div>
        <?php
        endif;
    }
}

?>
