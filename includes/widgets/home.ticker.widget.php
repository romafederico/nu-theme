<?php


class home_ticker extends WP_Widget {

    function __construct() {
		parent::__construct('home_ticker', 'Ticker Noticias', array('description' => __('Ticker de 30 noticias para la Home'),
            'post1' => '',
            'post2' => '',
            'post3' => '',
            'post4' => '',
            'post5' => '',
            'post6' => '',
            'post7' => '',
            'post8' => '',
            'post9' => '',
            'post10' => '',
            'post11' => '',
            'post12' => '',
            'post13' => '',
            'post14' => '',
            'post15' => '',
            'post16' => '',
            'post17' => '',
            'post18' => '',
            'post19' => '',
            'post20' => '',
            'post21' => '',
            'post22' => '',
            'post23' => '',
            'post24' => '',
            'post25' => '',
            'post26' => '',
            'post27' => '',
            'post28' => '',
            'post29' => '',
            'post30' => '',
            )
        );
	}

    public function form($instance) {
        if($instance) {
	        $post1 = (isset($instance['post1']))?intval($instance['post1']):'';
	        $post2 = (isset($instance['post2']))?intval($instance['post2']):'';
	        $post3 = (isset($instance['post3']))?intval($instance['post3']):'';
	        $post4 = (isset($instance['post4']))?intval($instance['post4']):'';
	        $post5 = (isset($instance['post5']))?intval($instance['post5']):'';
	        $post6 = (isset($instance['post6']))?intval($instance['post6']):'';
	        $post7 = (isset($instance['post7']))?intval($instance['post7']):'';
	        $post8 = (isset($instance['post8']))?intval($instance['post8']):'';
	        $post9 = (isset($instance['post9']))?intval($instance['post9']):'';
	        $post10 = (isset($instance['post10']))?intval($instance['post10']):'';
            $post11 = (isset($instance['post11']))?intval($instance['post11']):'';
	        $post12 = (isset($instance['post12']))?intval($instance['post12']):'';
	        $post13 = (isset($instance['post13']))?intval($instance['post13']):'';
	        $post14 = (isset($instance['post14']))?intval($instance['post14']):'';
	        $post15 = (isset($instance['post15']))?intval($instance['post15']):'';
	        $post16 = (isset($instance['post16']))?intval($instance['post16']):'';
	        $post17 = (isset($instance['post17']))?intval($instance['post17']):'';
	        $post18 = (isset($instance['post18']))?intval($instance['post18']):'';
	        $post19 = (isset($instance['post19']))?intval($instance['post19']):'';
	        $post20 = (isset($instance['post20']))?intval($instance['post20']):'';
            $post21 = (isset($instance['post21']))?intval($instance['post21']):'';
	        $post22 = (isset($instance['post22']))?intval($instance['post22']):'';
	        $post23 = (isset($instance['post23']))?intval($instance['post23']):'';
	        $post24 = (isset($instance['post24']))?intval($instance['post24']):'';
	        $post25 = (isset($instance['post25']))?intval($instance['post25']):'';
	        $post26 = (isset($instance['post26']))?intval($instance['post26']):'';
	        $post27 = (isset($instance['post27']))?intval($instance['post27']):'';
	        $post28 = (isset($instance['post28']))?intval($instance['post28']):'';
	        $post29 = (isset($instance['post29']))?intval($instance['post29']):'';
	        $post30 = (isset($instance['post30']))?intval($instance['post30']):'';

        }

	    ?>
        <p>Seleccionar todas las noticias que deben estar incluidas en la home</p>

        <p>
            <label for="<?php echo $this->get_field_id('post1');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post1'), $this->get_field_name('post1'), $post1.'_autocomplete', $post1, get_post_field('post_title',$post1)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post2');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post2'), $this->get_field_name('post2'), $post2.'_autocomplete', $post2, get_post_field('post_title',$post2)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post3');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post3'), $this->get_field_name('post3'), $post3.'_autocomplete', $post3, get_post_field('post_title',$post3)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post4');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post4'), $this->get_field_name('post4'), $post4.'_autocomplete', $post4, get_post_field('post_title',$post4)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post5');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post5'), $this->get_field_name('post5'), $post5.'_autocomplete', $post5, get_post_field('post_title',$post5)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post6');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post6'), $this->get_field_name('post6'), $post6.'_autocomplete', $post6, get_post_field('post_title',$post6)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post7');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post7'), $this->get_field_name('post7'), $post7.'_autocomplete', $post7, get_post_field('post_title',$post7)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post8');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post8'), $this->get_field_name('post8'), $post8.'_autocomplete', $post8, get_post_field('post_title',$post8)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post9');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post9'), $this->get_field_name('post9'), $post9.'_autocomplete', $post9, get_post_field('post_title',$post9)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post10');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post10'), $this->get_field_name('post10'), $post10.'_autocomplete', $post10, get_post_field('post_title',$post10)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post11');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post11'), $this->get_field_name('post11'), $post11.'_autocomplete', $post11, get_post_field('post_title',$post11)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post12');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post12'), $this->get_field_name('post12'), $post12.'_autocomplete', $post12, get_post_field('post_title',$post12)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post13');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post13'), $this->get_field_name('post13'), $post13.'_autocomplete', $post13, get_post_field('post_title',$post13)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post14');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post14'), $this->get_field_name('post14'), $post14.'_autocomplete', $post14, get_post_field('post_title',$post14)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post15');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post15'), $this->get_field_name('post15'), $post15.'_autocomplete', $post15, get_post_field('post_title',$post15)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post16');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post16'), $this->get_field_name('post16'), $post16.'_autocomplete', $post16, get_post_field('post_title',$post16)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post17');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post17'), $this->get_field_name('post17'), $post17.'_autocomplete', $post17, get_post_field('post_title',$post17)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post18');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post18'), $this->get_field_name('post18'), $post18.'_autocomplete', $post18, get_post_field('post_title',$post18)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post19');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post19'), $this->get_field_name('post19'), $post19.'_autocomplete', $post19, get_post_field('post_title',$post19)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post20');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post20'), $this->get_field_name('post20'), $post20.'_autocomplete', $post20, get_post_field('post_title',$post20)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post21');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post21'), $this->get_field_name('post21'), $post21.'_autocomplete', $post21, get_post_field('post_title',$post21)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post22');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post22'), $this->get_field_name('post22'), $post22.'_autocomplete', $post22, get_post_field('post_title',$post22)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post23');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post23'), $this->get_field_name('post23'), $post23.'_autocomplete', $post23, get_post_field('post_title',$post23)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post24');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post24'), $this->get_field_name('post24'), $post24.'_autocomplete', $post24, get_post_field('post_title',$post24)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post25');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post25'), $this->get_field_name('post25'), $post25.'_autocomplete', $post25, get_post_field('post_title',$post25)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post26');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post26'), $this->get_field_name('post26'), $post26.'_autocomplete', $post26, get_post_field('post_title',$post26)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post_L');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post_L'), $this->get_field_name('post_L'), $post_L.'_autocomplete', $post_L, get_post_field('post_title',$post_L)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post_L');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post_L'), $this->get_field_name('post_L'), $post_L.'_autocomplete', $post_L, get_post_field('post_title',$post_L)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post_L');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post_L'), $this->get_field_name('post_L'), $post_L.'_autocomplete', $post_L, get_post_field('post_title',$post_L)); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('post_L');?>">Noticia Izquierda</label>
		    <?php auto_complete('noticia', $this->get_field_id('post_L'), $this->get_field_name('post_L'), $post_L.'_autocomplete', $post_L, get_post_field('post_title',$post_L)); ?>
        </p>


        <?php

    }

    public function update($new_instance, $old_instance) {
	    $instance = $old_instance;
	    $instance['post1'] = intval( $new_instance['post1'] );
        $instance['post2'] = intval( $new_instance['post2'] );
        $instance['post3'] = intval( $new_instance['post3'] );
        $instance['post4'] = intval( $new_instance['post4'] );
        $instance['post5'] = intval( $new_instance['post5'] );
        $instance['post6'] = intval( $new_instance['post6'] );
        $instance['post7'] = intval( $new_instance['post7'] );
        $instance['post8'] = intval( $new_instance['post8'] );
        $instance['post9'] = intval( $new_instance['post9'] );
        $instance['post10'] = intval( $new_instance['post10'] );
        $instance['post11'] = intval( $new_instance['post11'] );
        $instance['post12'] = intval( $new_instance['post12'] );
        $instance['post13'] = intval( $new_instance['post13'] );
        $instance['post14'] = intval( $new_instance['post14'] );
        $instance['post15'] = intval( $new_instance['post15'] );
        $instance['post16'] = intval( $new_instance['post16'] );
        $instance['post17'] = intval( $new_instance['post17'] );
        $instance['post18'] = intval( $new_instance['post18'] );
        $instance['post19'] = intval( $new_instance['post19'] );
        $instance['post20'] = intval( $new_instance['post20'] );
        $instance['post21'] = intval( $new_instance['post21'] );
        $instance['post22'] = intval( $new_instance['post22'] );
        $instance['post23'] = intval( $new_instance['post23'] );
        $instance['post24'] = intval( $new_instance['post24'] );
        $instance['post25'] = intval( $new_instance['post25'] );
        $instance['post26'] = intval( $new_instance['post26'] );
        $instance['post27'] = intval( $new_instance['post27'] );
        $instance['post28'] = intval( $new_instance['post28'] );
        $instance['post29'] = intval( $new_instance['post29'] );
        $instance['post30'] = intval( $new_instance['post30'] );
	    return $instance;
    }

    public function widget($args, $instance) {
	    $post1 = apply_filters('post1', $instance['post1'] );
	    $post2 = apply_filters('post2', $instance['post2'] );
	    $post3 = apply_filters('post3', $instance['post3'] );
	    $post4 = apply_filters('post4', $instance['post4'] );
	    $post5 = apply_filters('post5', $instance['post5'] );
	    $post6 = apply_filters('post6', $instance['post6'] );
	    $post7 = apply_filters('post7', $instance['post7'] );
	    $post8 = apply_filters('post8', $instance['post8'] );
	    $post9 = apply_filters('post9', $instance['post9'] );
	    $post10 = apply_filters('post10', $instance['post10'] );
	    $post11 = apply_filters('post11', $instance['post11'] );
	    $post12 = apply_filters('post12', $instance['post12'] );
	    $post13 = apply_filters('post13', $instance['post13'] );
	    $post14 = apply_filters('post14', $instance['post14'] );
	    $post15 = apply_filters('post15', $instance['post15'] );
	    $post16 = apply_filters('post16', $instance['post16'] );
	    $post17 = apply_filters('post17', $instance['post17'] );
	    $post18 = apply_filters('post18', $instance['post18'] );
	    $post19 = apply_filters('post19', $instance['post19'] );
	    $post20 = apply_filters('post20', $instance['post20'] );
	    $post21 = apply_filters('post21', $instance['post21'] );
	    $post22 = apply_filters('post22', $instance['post22'] );
	    $post23 = apply_filters('post23', $instance['post23'] );
	    $post24 = apply_filters('post24', $instance['post24'] );
	    $post25 = apply_filters('post25', $instance['post25'] );
	    $post26 = apply_filters('post26', $instance['post26'] );
	    $post27 = apply_filters('post27', $instance['post27'] );
	    $post28 = apply_filters('post28', $instance['post28'] );
	    $post29 = apply_filters('post29', $instance['post29'] );
	    $post30 = apply_filters('post30', $instance['post30'] );

	    ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#lightSlider-Ticker").lightSlider({
                    item: 1,
                    autoWidth: false,
                    slideMove: 1, // slidemove will be 1 if loop is true
                    slideMargin: 5,

                    addClass: '',
                    mode: "fade",
                    useCSS: true,
                    cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                    easing: 'linear', //'for jquery animation',////

                    speed: 1000, //ms'
                    auto: true,
                    loop: true,
                    slideEndAnimation: true,
                    pause: 4000,

                    keyPress: true,
                    controls: true,
                    prevHtml: '<img style= "margin-top: 9px;" height="15px" src="<?php bloginfo('stylesheet_directory');?>/images/arrow-prev.svg" />',
                    nextHtml: '<img style= "margin-top: 8px;" height="15px" src="<?php bloginfo('stylesheet_directory');?>/images/arrow-next.svg" />',

                    rtl:false,
                    adaptiveHeight:false,

                    vertical:false,
                    verticalHeight:500,
                    vThumbWidth:100,

                    thumbItem:5,
                    pager: false,
                    gallery: false,
                    galleryMargin: 5,
                    thumbMargin: 5,
                    currentPagerPosition: 'middle',

                    enableTouch:true,
                    enableDrag:false,
                    freeMove:true,
                    swipeThreshold: 40,

                    responsive : [],

                    onBeforeStart: function (el) {},
                    onSliderLoad: function (el) {},
                    onBeforeSlide: function (el) {},
                    onAfterSlide: function (el) {},
                    onBeforeNextSlide: function (el) {},
                    onBeforePrevSlide: function (el) {}
                });
            });
        </script>


        <div class="row ticker">
            <div class="container" height="40px">
                <div class="col-xs-12 col-sm-12 col-md-2 gutter-sm ticker-title">
                    ÚLTIMAS NOTICIAS
                </div>
                <div class="col-xs-12 col-sm-12 col-md-10 gutter-sm">
                    <ul id="lightSlider-Ticker">
                        <?php

                        $noticiasTicker = [];

                        for ($i = 1; $i <= 30; $i++ ) {
                            if($instance) {
                                ${'post'.$i} = apply_filters('post'.$i, $instance['post'.$i]);
                                array_push($noticiasTicker, ${'post'.$i});
                            }
                        }

                        $args = array(
                            'post__in' => $noticiasTicker,
                            'post_type' => 'noticia',
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'post_status' => 'publish'
                        );

                        $query = new WP_Query($args);
                        if($query->have_posts()) :
                            while($query->have_posts()) : $query->the_post();
                                ?>
                                <div class="ticker-item">
                                    <li><a href="<?php echo get_post_permalink(); ?>"><h5><?php echo get_the_title(); ?></h5></a></li>
                                </div>
                                <?php
                            endwhile;
                            wp_reset_postdata();
                        endif;

                        ?>
                    </ul>
                </div>
            </div>
        </div>

	    <?php
    }
}

?>
