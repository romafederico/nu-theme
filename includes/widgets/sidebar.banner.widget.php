<?php

class banner_sidebar extends WP_Widget {

	function __construct() {
		parent::__construct('banner_sidebar', 'Banner Sidebar', array('description' => __('Formatos posibles: .jpg, .gif, .html'),
			'banner' => ''
			)
		);
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['banner'] = strip_tags($new_instance['banner']);
		return $instance;
	}


	// Backend part of the widget
	public function form($instance) {

		if($instance) {
			$banner = esc_attr($instance['banner']);
		} else {

		}

		?>
			<p>
				<h4>Tamaño: 300px de ancho x 250px de alto</h4>
                <label for="<?php echo $this->get_field_id('banner');?>">Seleccione Banner</label>
                <?= auto_complete('banner', $this->get_field_id('banner'), $this->get_field_name('banner'), $banner.'_autocomplete', $banner, get_post_field('post_title',$banner)); ?>
        </p>

		<?php
	}

	// Frontend part of the widget
	function widget($args, $instance) {
		$banner = apply_filters('banner', $instance['banner']);
		$banner_link = get_post_meta($banner, 'link_meta_url', true);
		$banner_img = get_post_meta($banner, 'banner_meta_tag', true);

		echo '<div class="banner-item"><a href="' . $banner_link . '" target="_blank">'.$banner_img.'</a></div>';
	}
}

?>
