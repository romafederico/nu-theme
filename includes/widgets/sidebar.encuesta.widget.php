<?php

class sidebar_encuesta extends WP_Widget {

    function __construct() {
		parent::__construct('sidebar_encuesta', 'Encuesta', array('description' => __('Encuestas para ubicar en el sidebar')));
	}

    public function form($instance) {
        global $wpdb;
        $poll_id = ($instance['poll_id'])?intval($instance['poll_id']):'';
        ?>
		<p>Seleccione la Encuesta que desea Mostrar en esta posición:</p>
		<p>
			<label for="<?= $this->get_field_id('poll_id'); ?>"><?php _e('Poll To Display:', 'wp-polls'); ?>
				<select name="<?= $this->get_field_name('poll_id'); ?>" id="<?= $this->get_field_id('poll_id'); ?>" class="widefat">
					<option value="-1"<?php selected(-1, $poll_id); ?>><?php _e('Do NOT Display Poll (Disable)', 'wp-polls'); ?></option>
					<option value="-2"<?php selected(-2, $poll_id); ?>><?php _e('Display Random Poll', 'wp-polls'); ?></option>
					<option value="0"<?php selected(0, $poll_id); ?>><?php _e('Display Latest Poll', 'wp-polls'); ?></option>
					<optgroup>&nbsp;</optgroup>
                    <?php
                    $polls = $wpdb->get_results("SELECT pollq_id, pollq_question FROM $wpdb->pollsq ORDER BY pollq_id DESC");
                    if ($polls):
                        foreach ($polls as $poll):
                            $pollq_question = stripslashes($poll->pollq_question);
                            $pollq_id = intval($poll->pollq_id);
                            ?>
		                    <option value="<?=$pollq_id; ?>" <?= selected($pollq_id, $poll_id); ?> ><?=$pollq_question; ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
				</select>
			</label>
		</p>
        <?php

    }

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['poll_id'] = intval($new_instance['poll_id']);
        return $instance;
    }

    public function widget($args, $instance) {
        extract($args);
        $poll_id = intval($instance['poll_id']);
        ?>
        <div class="encuesta">
            <div class="tit">Encuesta</div>
            <?php get_poll($poll_id); ?>
        </div>
        <?php
    }
}

?>
