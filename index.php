<?php get_header(); ?>

<div class="main-content">
    <?php if(is_front_page()) {
        dynamic_sidebar('widgets-home');
	} ?>
</div>

<?php get_footer(); ?>
