jQuery(document).ready(function(){
    jQuery('.titulo').each(function(){
        var hei=jQuery(this).height()+8;
        jQuery(this).css({'marginTop':(hei*-1)});
    });
});

function getAutocompleteWidgetSource(post_type, exclude_class){
    var source = ajaxurl+'?action=autocomplete_widgets&post_type='+post_type;
    source = source + '&exclude=' + jQuery.map(jQuery(exclude_class),function(i){ if(i.value!=''&&i.value!=0) return i.value; }).join('-');
    return source;
}

function autocomplete_widget(field_id,post_type,exclude_class){
    jQuery('#'+field_id).autocomplete({
        source: getAutocompleteWidgetSource(post_type,exclude_class),
        minLength: 3,
        //response: function( event, ui ) { },
        select: function( event, ui ) {
            //console.log(ui)
            jQuery('#'+field_id+'_id').val(ui.item.id);
            jQuery('#'+field_id+'_title').html(ui.item.value);

            var keydown = false
            var origEvent = event;
            while (origEvent.originalEvent !== undefined)
                origEvent = origEvent.originalEvent;
            if (origEvent.type == 'keydown')
                keydown = true

            if(!keydown){
                jQuery('#'+field_id).change();
                jQuery('#'+field_id).trigger("change");

            }

            jQuery('#'+field_id).hide();
            jQuery('#'+field_id+'_title').show();
            jQuery('#'+field_id+'_cambiar').show();
        }
    });

    jQuery('#'+field_id+'_cambiar').click(function() {
        jQuery('#'+field_id+'_title').hide();
        jQuery('#'+field_id+'_cambiar').hide();
        //jQuery('#'+field_id+'_id').val('');
        //jQuery('#'+field_id).val('');
        jQuery('#'+field_id).show();
    });
}

function trackChange(obj){
    //console.log("afuera")
}