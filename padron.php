<?php /*
Template Name: Padron Electoral
*/
?>

<?php get_header(); ?>

<div class="main-content">
	<div class="row post-header hidden-xs hidden-sm">
		<div class="container">
			<h3>
                <strong>Padrón Electoral 2017</strong>
			</h3>
		</div>
	</div>
	<div class="container">
		<div class="row categoria">
			<div class="col-md-8">

                <img src="<?php bloginfo('stylesheet_directory');?>/images/padron-header.jpg" width="100%">

                <iframe src="http://www.padron.gob.ar" width="100%" height="1100px" frameborder="0"></iframe>

			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
