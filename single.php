<?php get_header(); ?>
<?php

if(have_posts()) :
	while(have_posts()) : the_post();
		global $post;
		$postType = get_post_type($post->ID);
		$date = ucfirst(get_the_date($d = '', $post->ID));
		$time = get_the_time($t = '', $post->ID);
		$title = get_the_title($post->ID);
		$subtitle = get_post_meta($post->ID, 'subtitulo_meta_texto', true);
		$copete = get_post_meta($post->ID, 'copete_meta_texto', true);
		$authorType = get_post_meta($post->ID,'redaccion_meta_opcion',true);
		if($authorType == 'autor') :
		  	$authorFullName = get_the_author_meta('display_name',$post->post_author);
		  	$authorAvatar = get_avatar_url($post->post_author,120);
		    $authorUrl = get_author_posts_url(get_post_field( 'post_author', $post->ID));
		else:
		  	$authorFullName = theme_options('redaccion');
		  	$authorAvatar = get_template_directory_uri() . '/images/icon-nu.svg';
        endif;
        $terms = wp_get_post_tags($post->ID);
    endwhile;
endif;


function getNoticiasRelacionadas($terms, $postType, $authorId) {
    $today = getdate();

    $toExclude = $authorId * -1;

	$tags = [];
	foreach ($terms as $term) {
		array_push($tags, $term->term_id);
	}

    $args = array(
        'author' => $toExclude,
        'post_type' => $postType,
        'tag__in' => $tags,
        'posts_per_page' => 4,
        'post_status' => 'publish',
        'orderby' => 'rand',
        'date_query' => array(
            array(
                'year'  => $today['year'],
            ),
        ),
    );

	$query = new WP_Query($args);

	if($query->have_posts()) :

		switch ($postType) {

			case 'chicanu':
			case 'chiconu':
			break;

			// Loop getting related news post_type = 'noticia'
			case 'noticia':
				echo '<div class="relpost-title"><h3>Te puede interesar</h3></div>';
				echo '<div class="row relpost-itembox">';
				while($query->have_posts()) : $query->the_post();
					echo '<div class="col-sm-3 relpost-item gutter-nr">';
					echo '<a href="' . get_post_permalink() . '">';
					echo '<img src="' . get_the_post_thumbnail_url(get_the_ID(), 'sugeridas') . '" width="100%">';
					echo '<h5>' . get_the_title() . '</h5>';
					echo '</a>';
					echo '</div>';
				endwhile;
				echo '</div>';
				wp_reset_postdata();
			break;

			// Loop getting related news post_type = 'opinion'
			case 'opinion':
				echo '<div class="relpost-title"><h3>Más columnas de opinión</h3></div>';
				echo '<div class="row relpost-itembox">';
				while($query->have_posts()) : $query->the_post();
					echo '<div class="col-sm-3 relpost-item gutter-nr">';
					echo '<a href="' . get_post_permalink() . '">';
					$nrAuthorAvatar = get_avatar_url(get_the_author_meta('ID'));
					echo '<div class="author-photo" style="background-image: url(' . $nrAuthorAvatar . ')"></div>';
					echo '<h4>' . get_the_title() . '</h4>';
					$nrAuthorFullName = get_the_author_meta('display_name', get_the_author_meta('ID'));
					echo '<p>Por ' . $nrAuthorFullName . '</p>';
					echo '</a>';
					echo '</div>';
				endwhile;
				echo '</div>';
				wp_reset_postdata();
			break;

			// Loop getting related news post_type = 'pasillo'
			case 'pasillo':
				echo '<div class="relpost-title"><h3>Otras de pasillo...</h3></div>';
				echo '<div class="row relpost-itembox">';
				while($query->have_posts()) : $query->the_post();
					echo '<div class="col-sm-3 relpost-item gutter-nr">';
					echo '<a href="' . get_post_permalink() . '">';
					echo '<img src="' . get_the_post_thumbnail_url(get_the_ID(), 'sugeridas') . '" width="100%">';
					echo '<h5>' . get_the_title() . '</h5>';
					echo '</a>';
					echo '</div>';
				endwhile;
				echo '</div>';
				wp_reset_postdata();
			break;
		}

	endif;

}

function getNoticiasDeAutor($post, $authorId, $postType) {
    $today = getdate();
	$args = array(
        'author__in' => $authorId,
		'post_type' => $postType,
		'posts_per_page' => 4,
        'post_status' => 'publish',
        'post__not_in' => array($post),
        'meta_query' => array(
            array(
                'key' => 'redaccion_meta_opcion',
                'value' => 'autor',
            ),
        ),
        'orderby' => 'rand',
        'date_query' => array(
            array(
                'year'  => $today['year'],
            ),
        ),
	);

	$query = new WP_Query($args);

	if($query->have_posts()) :

        // Loop getting related news post_type = 'noticia'
        switch ($postType) {

	        case 'chicanu':
	        case 'chiconu':
	        case 'pasillo':
	        case 'opinion':
		        break;

	        // Loop getting related news post_type = 'noticia'
	        case 'noticia':
	            ?>
                    <div class="relpost-container">
                        <div class="relpost-title"><h3>Más sobre este autor</h3></div>
                        <div class="row relpost-itembox">
                            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <div class="col-sm-3 relpost-item gutter-nr">
                                <a href="<?php echo get_permalink() ?>">
                                    <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'sugeridas'); ?>" width="100%">
                                    <h5><?php echo get_the_title() ?></h5>
                                </a>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                <?php
		        wp_reset_postdata();
		        break;
        }

    endif;
}

?>

<div class="main-content">
	<div class="row post-header hidden-xs hidden-sm">
		<div class="container">
			<h3>
				<?php
				switch ($postType) {
					case 'noticia':
						$category = the_category(' | ','',$post->ID);
						echo $category;
					break;
					case 'pasillo':
						echo 'Pasillo Roverano';
					break;
					case 'opinion':
						echo 'Columna de Opinión';
					break;
					case 'chicanu':
						echo 'Chicas NU';
					break;
					case 'chiconu':
                		echo 'Chicos NU';
					break;
				}
				?>
			</h3>
		</div>
	</div>
	<div class="container">
		<div class="row post">
			<div class="col-md-8">
				<div class="post-date"><?php echo $date; echo ' | ' . $time; ?></div>
				<h1 class="hidden-xs"><?php echo $title ?></h1>
				<h2 class="visible-xs"><?php echo $title ?></h2>
				<h3><?php echo $subtitle ?></h3>
				<h4><?php echo $copete ?></h4>
				<?php
//
                // Firma de autor para noticias y opinion. Esto no se muestra si son Chicxs o Pasillo
				if ($postType != 'chicanu' && $postType != 'chiconu' && $postType != 'pasillo') {
					?>
                    <div class="post-author">

                        <div class="post-author-avatar">
                            <div class="author-photo" style="background-image: url(<?php echo $authorAvatar ?>)"></div>
                        </div>
                        <div class="post-author-name">
                            <?php
                            if ($authorType == 'autor') {
                                echo '<a href="' . $authorUrl . '"><h5>Por ' . $authorFullName . '</h5></a>';
                            } else {
                                echo '<h5>Por ' . $authorFullName . '</h5>';
                            }
                            ?>
                        </div>
                        <div class="post-social">
                            <ul>
                                <li><a target="_blank" href="https://facebook.com/sharer/sharer.php?&u=<?php echo get_permalink() ?>"><img src="<?php echo get_template_directory_uri() ?>/images/icon-share-fb.svg" height="23" alt="Facebook" /></a></li>
                                <li><a target="_blank" href="https://twitter.com/share?url=<?php echo get_permalink() ?>"><img src="<?php echo get_template_directory_uri() ?>/images/icon-share-tw.svg" height="25" alt="Twitter"></a></li>
                                <li><a href="whatsapp://send?text=<?php echo get_permalink() ?>" data-action="share/whatsapp/share"><img src="<?php echo get_template_directory_uri() ?>/images/icon-share-wapp.svg" height="25" alt="Twitter"></a></li>
                            </ul>
                        </div>
                    </div>
					<?php
				}

				switch(get_post_format()):
					case 'gallery'://imagen grande o galeria

						?>
						<script type="text/javascript">
							$(document).ready(function() {
								$("#lightSlider").lightSlider({
							item: 1,
									autoWidth: false,
									slideMove: 1, // slidemove will be 1 if loop is true
									slideMargin: 10,

									addClass: '',
									mode: "slide",
									useCSS: true,
									cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
									easing: 'linear', //'for jquery animation',////

									speed: 400, //ms'
									auto: true,
									loop: true,
									slideEndAnimation: true,
									pause: 9000,

									keyPress: true,
									controls: true,
									prevHtml: '<img style= "margin-left:20px;" height="35px" src="<?php bloginfo('stylesheet_directory');?>/images/arrow-prev.svg" />',
									nextHtml: '<img style= "margin:-20px;" height="35px" src="<?php bloginfo('stylesheet_directory');?>/images/arrow-next.svg" />',

									rtl:false,
									adaptiveHeight:false,

									vertical:false,
									verticalHeight:500,
									vThumbWidth:100,

									thumbItem:10,
									pager: false,
									gallery: false,
									galleryMargin: 5,
									thumbMargin: 5,
									currentPagerPosition: 'middle',

									enableTouch:true,
									enableDrag:false,
									freeMove:true,
									swipeThreshold: 40,

									responsive : [],

									onBeforeStart: function (el) {},
									onSliderLoad: function (el) {},
									onBeforeSlide: function (el) {},
									onAfterSlide: function (el) {},
									onBeforeNextSlide: function (el) {},
									onBeforePrevSlide: function (el) {}
								});
							});
						</script>
						<?php

						$items = get_post_meta($post->ID, 'galeria_meta_imagenes',true);

						$images = [];

						array_push($images, get_the_post_thumbnail_url($post->ID, 'single'));

						foreach ($items as $item) {
							array_push($images, wp_get_attachment_image_url($item, 'single'));
						}

						if(has_post_thumbnail($post->ID) || count($items)):

							echo '<div class="nota-slider">';
							echo '<ul id="lightSlider">';
							foreach($images as $image):
								echo '<li><img src="' . $image . '" width="100%"></li>';
							endforeach;
							echo '</ul>';
							echo '</div>';

						endif;
					break;

					case 'image'://imagen normal
						if (has_post_thumbnail($post->ID)):
							if ($postType != 'opinion') {
								echo '<div class="post-image">';
								echo '<img src="' . get_the_post_thumbnail_url($post->ID, 'single') . '" width="100%">';
								$imageCaption = get_post(get_post_thumbnail_id())->post_excerpt;
								if($imageCaption) {
								    echo '<div class="post-image-caption">' . $imageCaption . '</div>';
                                }
								echo '</div>';
							}
						endif;
					break;

					default://sin imagen
					break;

		        endswitch;
		        //fin de formatos de imagen

				?>
                <div class="visible-xs visible-sm">
                    <br>
                  <!--AdSense-->
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- nu-banner -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:336px;height:280px"
                             data-ad-client="ca-pub-9842752264387972"
                             data-ad-slot="9098508440"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                </div>
				<div class="post-content"><?php the_content($post->ID) ?></div>
			</div>
			<?php get_sidebar(); ?>

		</div>

		<?php

		if ($postType != 'chicanu' && $postType != 'chiconu' && $postType != 'pasillo') {
			?>
			<?php getNoticiasRelacionadas($terms, $postType, get_the_author_meta('ID')); ?>
            <?php if ($authorType != 'redaccion') : getNoticiasDeAutor($post->ID, get_the_author_meta('ID'), $postType); endif; ?>
            <div class="relpost-container">
				<div class="relpost-title"><h3>Qué se dice del tema...</h3></div>
				<div class="fb-comments" data-href="<?php get_permalink(); ?>" data-numposts="5"></div>
			</div>
			<?php
		}

			?>
	</div>
</div>

<?php get_footer(); ?>
