<?php /*
Template Name: Display All Authors
*/
?>

<?php get_header(); ?>

<div class="main-content">
	<div class="row post-header hidden-xs hidden-sm">
		<div class="container">
			<h3>
				Staff NU
			</h3>
		</div>
	</div>
	<div class="container">
		<div class="row categoria">
			<div class="col-md-12">

				<?php

				$args = array(
					'meta_key' => 'jerarquia',
					'meta_value' => '000',
					'meta_compare' => '!=',
					'orderby' => 'jerarquia',
					'order' => 'ASC',
				);

				$allUsers = get_users($args);

				foreach($allUsers as $user) {
					$userName = $user->display_name;

					?>
                    <div class="col-md-4">
                        <div class="staff-item ">
                            <a href="<?php echo get_author_posts_url($user->ID) ?>">
                                <div class="opi-author-avatar"><div class="opi-author-photo" style="background-image: url('<?php echo get_avatar_url($user->ID) ?>')"></div></div>
                                <h3><?php echo esc_html( $user->display_name ) ?></h3>
                            </a>
                            <h4><?php echo get_user_meta($user->ID, 'cargo', true); ?></h4>
                        </div>
                    </div>
                    <?php
				}
				?>

			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
