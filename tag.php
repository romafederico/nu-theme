<?php get_header(); ?>

<div class="main-content">
	<div class="row post-header hidden-xs hidden-sm">
		<div class="container">
			<h2>
				<?php echo $tag; ?>
			</h2>
		</div>
	</div>
	<div class="container">
		<div class="row categoria">
			<div class="col-md-8">
				<?php

				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

				$args = array(
					'post_type' => 'noticia',
					'posts_per_page' => 10,
					'tag' => $tag,
					'paged' => $paged,
					'orderby' => 'date',
					'order' => 'DESC',
					'post_status' => 'publish'
				);

				$query = new WP_Query($args);

				if($query->have_posts()) :
					while($query->have_posts()) : $query->the_post();
//						$authorAvatar = get_avatar_url(get_the_author_meta('ID'));
//						$authorFullName = get_the_author_meta('display_name', get_the_author_meta('ID'));

						if(get_post_meta($post->ID,'redaccion_meta_opcion',true)=='autor'):
							$authorFullName = get_the_author_meta('display_name',$post->post_author);
							$authorAvatar = get_avatar_url($post->post_author,120);
						    $authorUrl = get_author_posts_url(get_the_author_meta('ID'));
						else:
							$authorFullName = theme_options('redaccion');
							$authorAvatar = get_avatar_url(0,120);
						endif;

						$postPermalink = get_post_permalink();
						$postDate = get_the_date();
						$postThumbnail = get_the_post_thumbnail_url(get_the_ID());
						$postTitle = get_the_title();
						$postCopete = get_post_meta(get_the_ID(), 'copete_meta_texto', true);
						echo '<a href="' . $postPermalink . '">';
						echo '<div class="cat-item">';
						echo '<img src="' . $postThumbnail .'" width="100%">';
						echo '</a>';
						echo '<div class="cat-date hidden-xs"><p>' . $postDate . '</p></div>';
						echo '<a href="' . $authorUrl . '"><div class="cat-author-name"><h5>Por ' . $authorFullName . '</h5></div>';
						echo '<div class="cat-itembox">';
						echo '<div class="cat-author-avatar"><div class="author-photo" style="background-image: url(' . $authorAvatar . ')"></div></div></div></a>';
						echo '</div>';
						echo '<a href="' . $postPermalink . '">';
						echo '<div class="cat-item-title"><h3>' . $postTitle . '</h3>';
						echo '<p>' . $postCopete . '</p></div>';
						echo '</a>';
						echo '<div class="cat-item-footer"><div class="cat-item-footer-line"></div>';
						echo '<div class="cat-item-footer-social"><ul>';
						echo '<li><a href="https://facebook.com/sharer/sharer/php?&u=' . $postPermalink . '" target="_blank"><img src="' . get_template_directory_uri() . '/images/icon-share-fb.svg" height="23" alt="Facebook" /></a></li>';
						echo '<li><a href="https://twitter.com/share?url=' . $postPermalink . '" target="_blank"><img src="' . get_template_directory_uri() . '/images/icon-share-tw.svg" height="25" alt="Twitter"></a></li>';
						echo '<li><a href="whatsapp://send?text=' . $postPermalink . '" data-action="share/whatsapp/share"><img src="' . get_template_directory_uri() . '/images/icon-share-wapp.svg" height="25" alt="WhatsApp"></a></li>';
						echo '</ul></div></div>';
					endwhile;

					wp_reset_postdata();

					// next_posts_link('Older Entries',$query->max_num_pages);
					// previous_posts_link('Newer Entries');

				endif;
				?>
			</div>

			<?php get_sidebar(); ?>
		</div>
	</div>
</div>



<?php get_footer(); ?>
